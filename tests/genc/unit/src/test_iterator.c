#include <gtest/gtest.h>
#include <genc/iterator.h>
#include <genc/slist.h>
#include <genc/list.h>
#include <genc/vector.h>

TEST(test_advance_, forward_iterator)
{
	gc_slist sl;
	gc_slist_iterator slb;
	int i = 1, j = 2;
	gc_slist_init(&sl, sizeof(i));
	sl.push_front(&sl, &i);
	sl.push_front(&sl, &j);
	slb = sl.begin(&sl);
    ASSERT_TRUE(*((int *)((gc_iterator *)&slb)->value) == j) <<
        "Iterator value incorrectly set.";
	gc_advance(&slb);
    ASSERT_TRUE(*((int *)((gc_iterator *)&slb)->value) == i) <<
        "Iterator value incorrectly set.";
	gc_slist_uinit(&sl);
}

TEST(test_advance_, bidirectional_iterator)
{
	gc_list l;
	gc_list_iterator lb;
	int i = 1, j = 2;
	gc_list_init(&l, sizeof(i));
	l.push_back(&l, &i);
	l.push_back(&l, &j);
	lb = l.begin(&l);
    ASSERT_TRUE(*((int *)((gc_iterator *)&lb)->value) == i) <<
        "Iterator value incorrectly set.";
	gc_advance(&lb);
    ASSERT_TRUE(*((int *)((gc_iterator *)&lb)->value) == j) <<
        "Iterator value incorrectly set.";
	gc_list_uinit(&l);
}

TEST(test_advance_, random_access_iterator)
{
	gc_vector v;
	gc_vector_iterator vb;
	int i = 1, j = 2;
	gc_vector_init(&v, sizeof(i));
	v.push_back(&v, &i);
	v.push_back(&v, &j);
	vb = v.begin(&v);
    ASSERT_TRUE(*((int *)((gc_iterator *)&vb)->value) == i) <<
        "Iterator value incorrectly set.";
	gc_advance(&vb);
    ASSERT_TRUE(*((int *)((gc_iterator *)&vb)->value) == j) <<
        "Iterator value incorrectly set.";
	gc_vector_uinit(&v);
}

TEST(test_radvance, bidirectional_iterator)
{
	gc_list l;
	gc_list_iterator lb;
	int i = 2, j = 2;
	gc_list_init(&l, sizeof(i));
	l.push_back(&l, &i);
	l.push_back(&l, &j);
	lb = l.begin(&l);
    ASSERT_TRUE(*((int *)((gc_iterator *)&lb)->value) == i) <<
        "Iterator value incorrectly set.";
	gc_advance(&lb);
    ASSERT_TRUE(*((int *)((gc_iterator *)&lb)->value) == j) <<
        "Iterator value incorrectly set.";
	gc_radvance(&lb);
    ASSERT_TRUE(*((int *)((gc_iterator *)&lb)->value) == i) <<
        "Iterator value incorrectly set.";
	gc_list_uinit(&l);
}

TEST(test_radvance, random_access_iterator)
{
	gc_vector v;
	gc_vector_iterator vb;
	int i = 1, j = 2;
	gc_vector_init(&v, sizeof(i));
	v.push_back(&v, &i);
	v.push_back(&v, &j);
	vb = v.begin(&v);
    ASSERT_TRUE(*((int *)((gc_iterator *)&vb)->value) == i) <<
        "Iterator value incorrectly set.";
	gc_advance(&vb);
    ASSERT_TRUE(*((int *)((gc_iterator *)&vb)->value) == j) <<
        "Iterator value incorrectly set.";
	gc_radvance(&vb);
    ASSERT_TRUE(*((int *)((gc_iterator *)&vb)->value) == i) <<
        "Iterator value incorrectly set.";
	gc_vector_uinit(&v);
}

TEST(test_dadvance, random_access_iterator)
{
	gc_vector v;
	gc_vector_iterator vb;
	int i = 1, j = 2, k = 3;
	gc_vector_init(&v, sizeof(i));
	v.push_back(&v, &i);
	v.push_back(&v, &j);
	v.push_back(&v, &k);
	vb = v.begin(&v);
    ASSERT_TRUE(*((int *)((gc_iterator *)&vb)->value) == i) <<
        "Iterator value incorrectly set.";
	gc_dadvance(&vb, 2);
    ASSERT_TRUE(*((int *)((gc_iterator *)&vb)->value) == k) <<
        "Iterator value incorrectly set.";
	gc_dadvance(&vb, -1);
    ASSERT_TRUE(*((int *)((gc_iterator *)&vb)->value) == j) <<
        "Iterator value incorrectly set.";
	gc_vector_uinit(&v);
}

TEST(test_value, size)
{
	gc_vector v;
	gc_vector_iterator vb;
	gc_vector_init(&v, sizeof(int));
	vb = v.begin(&v);
    ASSERT_TRUE(((gc_iterator *)&vb)->value_size == sizeof(int)) <<
        "Iterator value_size incorrectly set.";
    ASSERT_TRUE(((gc_iterator *)&vb)->value_size == ((gc_allocator *)&v)->elem_size) <<
        "Iterator value_size different to allocator value_size.";
	gc_vector_uinit(&v);
}
