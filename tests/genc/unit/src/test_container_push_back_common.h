#ifndef TEST_CONTAINER_PUSH_BACK_COMMON_H
#define TEST_CONTAINER_PUSH_BACK_COMMON_H

#include <stddef.h>
#include <gtest/gtest.h>
#include <genc/iterator.h>

extern void (*test_container_init)(test_container *, size_t);
extern void (*test_container_uinit)(test_container *);
extern const int num_of_multiple_elems;

void test_push_back_empty_common()
{
	int i = 1;
	test_container c;
	test_container_iterator cb;
	test_container_init(&c, sizeof(int));
	c.push_back(&c, &i);
	cb = c.begin(&c);
        ASSERT_TRUE(*((int *)((gc_iterator *)&cb)->value) == i) <<
            "Begin iterator value incorrect.";
	test_container_uinit(&c);
}

void test_push_back_single_element_common()
{
	int first = 1, i = first;
	test_container c;
	test_container_iterator cb;
	test_container_init(&c, sizeof(int));
	c.push_back(&c, &i);
	++i;
	c.push_back(&c, &i);
	cb = c.begin(&c);
        ASSERT_TRUE(*((int *)((gc_iterator *)&cb)->value) == first) <<
            "Begin iterator value incorrect.";
	test_container_uinit(&c);
}

#endif
