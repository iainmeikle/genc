#include <gtest/gtest.h>
#include <genc/queue.h>

typedef gc_queue test_container;
typedef gc_queue_iterator test_container_iterator;

#include "test_container_back_push_back_common.h"
#include "test_container_begin_common.h"
#include "test_container_begin_push_back_common.h"
#include "test_container_end_common.h"
#include "test_container_end_push_back_common.h"
#include "test_container_front_push_back_common.h"
#include "test_container_pop_front_push_back_common.h"
#include "test_container_push_back_common.h"
#include "test_container_reserve_common.h"
#include "test_container_reserve_push_back_common.h"
#include "test_container_resize_common.h"
#include "test_container_resize_push_back_common.h"

void (*test_container_init)(test_container *, size_t) = &gc_queue_init;
void (*test_container_uinit)(test_container *) = &gc_queue_uinit;

const int num_of_multiple_elems = 100;



TEST(test_back, single_element)
{
	test_back_single_element_push_back_common();
}

TEST(test_back, multiple_elements)
{
	test_back_multiple_elements_push_back_common();
}



TEST(test_begin, empty)
{
	test_begin_empty_common();
}

TEST(test_begin, not_empty)
{
	test_begin_not_empty_push_back_common();
}



TEST(test_end, empty)
{
	test_end_empty_common();
}

TEST(test_end, not_empty)
{
	test_end_not_empty_push_back_common();
}



TEST(test_front, single_element)
{
	test_front_single_element_push_back_common();
}

TEST(test_front, multiple_elements)
{
	test_front_multiple_elements_push_back_common();
}



TEST(test_pop_front, single_element)
{
	test_pop_front_single_element_push_back_common();
}

TEST(test_pop_front, multiple_elements)
{
	test_pop_front_multiple_elements_push_back_common();
}



TEST(test_push_back, empty)
{
	test_push_back_empty_common();
}

TEST(test_push_back, single_element)
{
	test_push_back_single_element_common();
}



TEST(test_reserve, decrease)
{
	test_reserve_decrease_push_back_common();
}

TEST(test_reserve, increase)
{
	test_reserve_increase_common();
}



TEST(test_resize, decrease)
{
	test_resize_decrease_push_back_common();
}

TEST(test_resize, increase)
{
	test_resize_increase_common();
}
