#ifndef TEST_CONTAINER_RESERVE_COMMON_H
#define TEST_CONTAINER_RESERVE_COMMON_H

#include <stddef.h>
#include <gtest/gtest.h>
#include <genc/iterator.h>

extern void (*test_container_init)(test_container *, size_t);
extern void (*test_container_uinit)(test_container *);
extern const int num_of_multiple_elems;

void test_reserve_increase_common()
{
	test_container c;
	test_container_init(&c, sizeof(int));
	c.reserve(&c, num_of_multiple_elems);
		ASSERT_TRUE(num_of_multiple_elems <= (long)gc_capacity(&c)) <<
            "Incorrect number of elements initially reserved.";
	c.reserve(&c, num_of_multiple_elems * 2);
		ASSERT_TRUE((num_of_multiple_elems * 2) <= (long)gc_capacity(&c)) <<
            "Reserved number of elements not increased correctly.";
	test_container_uinit(&c);
}

#endif
