#include <gtest/gtest.h>
#include <genc/slist.h>

typedef gc_slist test_container;
typedef gc_slist_iterator test_container_iterator;

#include "test_container_begin_common.h"
#include "test_container_begin_push_front_common.h"
#include "test_container_clear_push_front_common.h"
#include "test_container_end_common.h"
#include "test_container_end_push_front_common.h"
#include "test_container_front_push_front_common.h"
#include "test_container_pop_front_push_front_common.h"
#include "test_container_push_front_common.h"
#include "test_container_resize_common.h"
#include "test_container_resize_push_front_common.h"

void (*test_container_init)(test_container *, size_t) = &gc_slist_init;
void (*test_container_uinit)(test_container *) = &gc_slist_uinit;

const int num_of_multiple_elems = 100;



TEST(test_begin, empty)
{
	test_begin_empty_common();
}

TEST(test_begin, not_empty)
{
	test_begin_not_empty_push_front_common();
}



TEST(test_clear, empty)
{
	test_clear_empty_push_front_common();
}

TEST(test_clear, single_element)
{
	test_clear_single_element_push_front_common();
}

TEST(test_clear, multiple_elements)
{
	test_clear_multiple_elements_push_front_common();
}



TEST(test_end, empty)
{
	test_end_empty_common();
}

TEST(test_end, not_empty)
{
	test_end_not_empty_push_front_common();
}



TEST(test_front, single_element)
{
	test_front_single_element_push_front_common();
}

TEST(test_front, multiple_elements)
{
	test_front_multiple_elements_push_front_common();
}



TEST(test_pop_front, single_element)
{
	test_pop_front_single_element_common();
}

TEST(test_pop_front, multiple_elements)
{
	test_pop_front_multiple_elements_common();
}



TEST(test_push_front, empty)
{
	test_push_front_empty_common();
}

TEST(test_push_front, single_element)
{
	test_push_front_single_element_common();
}



TEST(test_resize, decrease)
{
	test_resize_decrease_push_front_common();
}

TEST(test_resize, increase)
{
	test_resize_increase_common();
}
