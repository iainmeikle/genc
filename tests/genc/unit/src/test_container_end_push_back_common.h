#ifndef TEST_CONTAINER_END_PUSH_BACK_COMMON_H
#define TEST_CONTAINER_END_PUSH_BACK_COMMON_H

#include <stddef.h>
#include <gtest/gtest.h>
#include <genc/iterator.h>

extern void (*test_container_init)(test_container *, size_t);
extern void (*test_container_uinit)(test_container *);
extern const int num_of_multiple_elems;

void test_end_not_empty_push_back_common()
{
	int i = 1;
	test_container c;
	test_container_iterator cb, ce;
	gc_iterator *b, * e;
	test_container_init(&c, sizeof(int));
	c.push_back(&c, &i);
	cb = c.begin(&c);
	ce = c.end(&c);
	gc_advance(&cb);
	b = (gc_iterator *)&cb;
	e = (gc_iterator *)&ce;
        ASSERT_TRUE(b->it == e->it) << "End iterator incorrect.";
	test_container_uinit(&c);
}

#endif
