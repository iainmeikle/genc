#ifndef TEST_CONTAINER_RBEGIN_PUSH_BACK_COMMON_H
#define TEST_CONTAINER_RBEGIN_PUSH_BACK_COMMON_H

#include <stddef.h>
#include <gtest/gtest.h>
#include <genc/iterator.h>

extern void (*test_container_init)(test_container *, size_t);
extern void (*test_container_uinit)(test_container *);
extern const int num_of_multiple_elems;

void test_rbegin_not_empty_common()
{
	int i = 1;
	test_container c;
	test_container_iterator crb;
	gc_iterator *b;
	test_container_init(&c, sizeof(int));
	c.push_back(&c, &i);
	++i;
	c.push_back(&c, &i);
	crb = c.rbegin(&c);
	b = (gc_iterator *)&crb;
        ASSERT_TRUE(b->it != 0) << "The begin iterator is null.";
        ASSERT_TRUE(*((int *)b->value) == i) << "The begin iterator value is incorrect.";
	test_container_uinit(&c);
}

#endif
