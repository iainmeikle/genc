#ifndef TEST_CONTAINER_REND_PUSH_BACK_COMMON_H
#define TEST_CONTAINER_REND_PUSH_BACK_COMMON_H

#include <stddef.h>
#include <gtest/gtest.h>
#include <genc/iterator.h>

extern void (*test_container_init)(test_container *, size_t);
extern void (*test_container_uinit)(test_container *);
extern const int num_of_multiple_elems;

void test_rend_not_empty_push_back_common()
{
	int i = 1;
	test_container c;
	test_container_iterator crb, cre;
	gc_iterator *b, * e;
	test_container_init(&c, sizeof(int));
	c.push_back(&c, &i);
	crb = c.rbegin(&c);
	cre = c.rend(&c);
	gc_advance(&crb);
	b = (gc_iterator *)&crb;
	e = (gc_iterator *)&cre;
        ASSERT_TRUE(b->it == e->it) << "End iterator incorrect.";
        ASSERT_TRUE(e->value == 0) << "End iterator incorrectly references a value.";
	test_container_uinit(&c);
}

#endif
