#ifndef TEST_CONTAINER_BACK_PUSH_BACK_COMMON_H
#define TEST_CONTAINER_BACK_PUSH_BACK_COMMON_H

#include <stddef.h>
#include <gtest/gtest.h>
#include <genc/iterator.h>

extern void (*test_container_init)(test_container *, size_t);
extern void (*test_container_uinit)(test_container *);
extern const int num_of_multiple_elems;

static void test_back_push_back(size_t nelems)
{
	size_t i;
	test_container c;
	test_container_init(&c, sizeof(size_t));

	for (i = 1; i <= nelems; ++i)
	{
		c.push_back(&c, &i);
	}

        ASSERT_TRUE(* ((size_t *)c.back(&c)) == nelems) <<
            "Incorrect value at the back of the vector.";

	test_container_uinit(&c);
}

void test_back_single_element_push_back_common()
{
	test_back_push_back(1);
}

void test_back_multiple_elements_push_back_common()
{
	test_back_push_back(num_of_multiple_elems);
}

#endif
