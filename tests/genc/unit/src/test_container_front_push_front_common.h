#ifndef TEST_CONTAINER_FRONT_PUSH_FRONT_COMMON_H
#define TEST_CONTAINER_FRONT_PUSH_FRONT_COMMON_H

#include <stddef.h>
#include <gtest/gtest.h>
#include <genc/iterator.h>

extern void (*test_container_init)(test_container *, size_t);
extern void (*test_container_uinit)(test_container *);
extern const int num_of_multiple_elems;

void test_front_single_element_push_front_common()
{
	int i = 1;
	test_container c;
	test_container_init(&c, sizeof(int));
	c.push_front(&c, &i);
        ASSERT_TRUE(*((int *)c.front(&c)) == i) <<
            "Front value incorrect.";
        test_container_uinit(&c);
}

void test_front_multiple_elements_push_front_common()
{
	int i;
	test_container c;
	test_container_init(&c, sizeof(int));

	for (i = 0; i < num_of_multiple_elems; ++i)
	{
		c.push_front(&c, &i);
                ASSERT_TRUE(*((int *)c.front(&c)) == i) <<
                    "Front value incorrect.";
	}

	test_container_uinit(&c);
}

#endif
