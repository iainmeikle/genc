#include <gtest/gtest.h>
#include <genc/list.h>

typedef gc_list test_container;
typedef gc_list_iterator test_container_iterator;

#include "test_container_back_push_back_common.h"
#include "test_container_begin_common.h"
#include "test_container_begin_push_back_common.h"
#include "test_container_end_common.h"
#include "test_container_end_push_back_common.h"
#include "test_container_clear_push_back_common.h"
#include "test_container_erase_common.h"
#include "test_container_front_push_front_common.h"
#include "test_container_insert_after_common.h"
#include "test_container_insert_before_common.h"
#include "test_container_pop_back_common.h"
#include "test_container_pop_front_push_back_common.h"
#include "test_container_push_back_common.h"
#include "test_container_push_front_common.h"
#include "test_container_rbegin_common.h"
#include "test_container_rbegin_push_back_common.h"
#include "test_container_rend_common.h"
#include "test_container_rend_push_back_common.h"
#include "test_container_resize_common.h"
#include "test_container_resize_push_back_common.h"

void (*test_container_init)(test_container *, size_t) = &gc_list_init;
void (*test_container_uinit)(test_container *) = &gc_list_uinit;

const int num_of_multiple_elems = 100;



TEST(test_back_, single_element)
{
	test_back_single_element_push_back_common();
}

TEST(test_back_, multiple_elements)
{
	test_back_multiple_elements_push_back_common();
}



TEST(test_begin, empty)
{
	test_begin_empty_common();
}

TEST(test_begin, not_empty)
{
	test_begin_not_empty_push_back_common();
}



TEST(test_clear, empty)
{
	test_clear_empty_push_back_common();
}

TEST(test_clear, single_element)
{
	test_clear_single_element_push_back_common();
}

TEST(test_clear, multiple_elements)
{
	test_clear_multiple_elements_push_back_common();
}



TEST(test_end, empty)
{
	test_end_empty_common();
}

TEST(test_end_not, empty)
{
	test_end_not_empty_push_back_common();
}



TEST(test_erase_, all_empty)
{
	test_erase_all_empty_common();
}

TEST(test_erase_, all_single_element)
{
	test_erase_all_single_element_common();
}

TEST(test_erase_, all_multiple_elements)
{
	test_erase_all_multiple_elements_common();
}

TEST(test_erase_, first_single_element_single_element)
{
	test_erase_first_single_element_single_element_common();
}

TEST(test_erase_, last_single_element_single_element)
{
	test_erase_last_single_element_single_element_common();
}

TEST(test_erase_, first_single_element_multiple_elements)
{
	test_erase_first_single_element_multiple_elements_common();
}

TEST(test_erase_, last_single_element_multiple_elements)
{
	test_erase_last_single_element_multiple_elements_common();
}

TEST(test_erase_, multiple_elements_start_multiple_elements)
{
	test_erase_multiple_elements_start_multiple_elements_common();
}

TEST(test_erase_, multiple_elements_end_multiple_elements)
{
	test_erase_multiple_elements_end_multiple_elements_common();
}



TEST(test_front, single_element)
{
	test_front_single_element_push_front_common();
}

TEST(test_front, multiple_elements)
{
	test_front_multiple_elements_push_front_common();
}



TEST(test_insert_after, begin_single_element)
{
	test_insert_after_begin_single_element_common();
}

TEST(test_insert_after, begin_multiple_elements)
{
	test_insert_after_begin_multiple_elements_common();
}

TEST(test_insert_after, rbegin_single_element)
{
	test_insert_after_rbegin_single_element_common();
}

TEST(test_insert_after, rbegin_multiple_elements)
{
	test_insert_after_rbegin_multiple_elements_common();
}

TEST(test_insert_after, rend_single_element)
{
	test_insert_after_rend_single_element_common();
}

TEST(test_insert_after, rend_multiple_elements)
{
	test_insert_after_rend_multiple_elements_common();
}



TEST(test_insert_before, begin_single_element)
{
	test_insert_before_begin_single_element_common();
}

TEST(test_insert_before, begin_multiple_elements)
{
	test_insert_before_begin_multiple_elements_common();
}

TEST(test_insert_before, end_single_element)
{
	test_insert_before_end_single_element_common();
}

TEST(test_insert_before, end_multiple_elements)
{
	test_insert_before_end_multiple_elements_common();
}

TEST(test_insert_before, rbegin_single_element)
{
	test_insert_before_rbegin_single_element_common();
}

TEST(test_insert_before, rbegin_multiple_elements)
{
	test_insert_before_rbegin_multiple_elements_common();
}



TEST(test_pop_back, single_element)
{
	test_pop_back_single_element_common();
}

TEST(test_pop_back, multiple_elements)
{
	test_pop_back_multiple_elements_common();
}



TEST(test_pop_front, single_element)
{
	test_pop_front_single_element_push_back_common();
}

TEST(test_pop_front, multiple_elements)
{
	test_pop_front_multiple_elements_push_back_common();
}



TEST(test_push_back, empty)
{
	test_push_back_empty_common();
}

TEST(test_push_back, single_element)
{
	test_push_back_single_element_common();
}



TEST(test_push_front, empty)
{
	test_push_front_empty_common();
}

TEST(test_push_front, single_element)
{
	test_push_front_single_element_common();
}



TEST(test_rbegin, empty)
{
	test_rbegin_empty_common();
}

TEST(test_rbegin, not_empty)
{
	test_rbegin_not_empty_common();
}



TEST(test_rend, empty)
{
	test_rend_empty_common();
}

TEST(test_rend, not_empty)
{
	test_rend_not_empty_push_back_common();
}



TEST(test_resize, decrease)
{
	test_resize_decrease_push_back_common();
}

TEST(test_resize, increase)
{
	test_resize_increase_common();
}
