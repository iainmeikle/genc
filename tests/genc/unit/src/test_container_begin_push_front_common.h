#ifndef TEST_CONTAINER_BEGIN_PUSH_FRONT_COMMON_H
#define TEST_CONTAINER_BEGIN_PUSH_FRONT_COMMON_H

#include <stddef.h>
#include <gtest/gtest.h>
#include <genc/iterator.h>

extern void (*test_container_init)(test_container *, size_t);
extern void (*test_container_uinit)(test_container *);
extern const int num_of_multiple_elems;

void test_begin_not_empty_push_front_common()
{
	int first = 1, i = first;
	test_container c;
	test_container_iterator cb;
	gc_iterator *b;
	test_container_init(&c, sizeof(int));
	c.push_front(&c, &i);
	++i;
	c.push_front(&c, &i);
	cb = c.begin(&c);
	b = (gc_iterator *)&cb;
        ASSERT_TRUE(b->it != 0) << "The begin iterator is null.";
        ASSERT_TRUE(*((int *)b->value) == i) <<
            "The begin iterator value is incorrect.";
	test_container_uinit(&c);
}

#endif
