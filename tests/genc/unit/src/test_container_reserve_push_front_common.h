#ifndef TEST_CONTAINER_RESERVE_PUSH_FRONT_COMMON_H
#define TEST_CONTAINER_RESERVE_PUSH_FRONT_COMMON_H

#include <stddef.h>
#include <gtest/gtest.h>
#include <genc/iterator.h>

extern void (*test_container_init)(test_container *, size_t);
extern void (*test_container_uinit)(test_container *);
extern const int num_of_multiple_elems;

void test_reserve_decrease_push_front_common()
{
	int i, hs = (num_of_multiple_elems / 2);
	test_container c;
	test_container_init(&c, sizeof(int));

	for (i = 0; i < num_of_multiple_elems; ++i)
	{
		c.push_front(&c, &i);
	}

	c.reserve(&c, hs);
		ASSERT_TRUE(num_of_multiple_elems <= (long)gc_capacity(&c)) <<
            "Reserved capacity incorrectly reduced.";
	test_container_uinit(&c);
}

#endif
