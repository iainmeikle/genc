#ifndef TEST_CONTAINER_ERASE_COMMON_H
#define TEST_CONTAINER_ERASE_COMMON_H

#include <stddef.h>
#include <gtest/gtest.h>
#include <genc/iterator.h>

extern void (*test_container_init)(test_container *, size_t);
extern void (*test_container_uinit)(test_container *);
extern const int num_of_multiple_elems;

static void test_erase_begin(size_t nelems, size_t advances)
{
	size_t i;
	test_container c;
	test_container_iterator cb, ce;
	test_container_init(&c, sizeof(i));

	for (i = 0; i < nelems; ++i)
	{
		c.push_back(&c, &i);
	}

	cb = c.begin(&c);
	ce = c.begin(&c);

	for (i = 0; i < advances; ++i)
	{
		gc_advance(&ce);
	}

	c.erase(&c, &cb, &ce);
		ASSERT_TRUE(gc_count(&c) == (nelems - advances)) <<
            "Incorrect count reported";
	test_container_uinit(&c);
}

static void test_erase_rbegin(size_t nelems, size_t advances)
{
	size_t i;
	test_container c;
	test_container_iterator crb, cre;
	test_container_init(&c, sizeof(i));

	for (i = 0; i < nelems; ++i)
	{
		c.push_back(&c, &i);
	}

	crb = c.rbegin(&c);
	cre = c.rbegin(&c);

	for (i = 0; i < advances; ++i)
	{
		gc_advance(&cre);
	}

	c.erase(&c, &crb, &cre);
		ASSERT_TRUE(gc_count(&c) == (nelems - advances)) <<
            "Incorrect count reported";
	test_container_uinit(&c);
}

void test_erase_all_empty_common()
{
	test_erase_begin(0, 0);
}

void test_erase_all_single_element_common()
{
	test_erase_begin(1, 0);
}

void test_erase_all_multiple_elements_common()
{
	test_erase_begin(num_of_multiple_elems, 0);
}

void test_erase_first_single_element_single_element_common()
{
	test_erase_begin(1, 1);
}

void test_erase_last_single_element_single_element_common()
{
	test_erase_rbegin(1, 1);
}

void test_erase_first_single_element_multiple_elements_common()
{
	test_erase_begin(num_of_multiple_elems, 1);
}

void test_erase_last_single_element_multiple_elements_common()
{
	test_erase_rbegin(num_of_multiple_elems, 1);
}

void test_erase_multiple_elements_start_multiple_elements_common()
{
	test_erase_begin(num_of_multiple_elems, (num_of_multiple_elems / 2));
}

void test_erase_multiple_elements_end_multiple_elements_common()
{
	test_erase_rbegin(num_of_multiple_elems, (num_of_multiple_elems / 2));
}

#endif
