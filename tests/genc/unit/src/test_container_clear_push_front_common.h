#ifndef TEST_CONTAINER_CLEAR_PUSH_FRONT_COMMON_H
#define TEST_CONTAINER_CLEAR_PUSH_FRONT_COMMON_H

#include <stddef.h>
#include <gtest/gtest.h>
#include <genc/iterator.h>

extern void (*test_container_init)(test_container *, size_t);
extern void (*test_container_uinit)(test_container *);
extern const int num_of_multiple_elems;

static void test_clear(size_t nelems)
{
	size_t i;
	test_container c;
	test_container_init(&c, sizeof(size_t));

	for (i = 0; i < nelems; ++i)
	{
		c.push_front(&c, &i);
	}

		ASSERT_TRUE(gc_count(&c) == nelems) << "Count reported incorrectly.";
	c.clear(&c);

		ASSERT_TRUE(gc_count(&c) == 0) << "Container not cleared.";
	test_container_uinit(&c);
}

void test_clear_empty_push_front_common()
{
	test_clear(0);
}

void test_clear_single_element_push_front_common()
{
	test_clear(1);
}

void test_clear_multiple_elements_push_front_common()
{
	test_clear(num_of_multiple_elems);
}

#endif
