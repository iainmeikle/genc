#include <gtest/gtest.h>
#include <genc/container.h>
#include <genc/list.h>
#include <genc/queue.h>
#include <genc/slist.h>
#include <genc/stack.h>
#include <genc/vector.h>

static const int num_of_multiple_elems = 100;



TEST(test_capacity, gc_empty)
{
	gc_vector v;
	gc_vector_init(&v, sizeof(int));
	ASSERT_TRUE(gc_capacity(&v) == 0U) << "Vector reports incorrect capacity.";
	gc_vector_uinit(&v);
}

TEST(test_capacity, incremental)
{
	int i = 0;
	gc_vector v;
	gc_vector_init_allocator(&v, gc_default_allocator(sizeof(i)), 2.0);
	v.push_back(&v, &i);
	v.push_back(&v, &i);
	v.push_back(&v, &i);
    ASSERT_TRUE(gc_capacity(&v) == 4) << "Vector reports incorrect capacity.";
	gc_vector_uinit(&v);
}

TEST(test_capacity, single_element)
{
	int i = 0;
	gc_vector v;
	gc_vector_init(&v, sizeof(i));
	v.push_back(&v, &i);
    ASSERT_TRUE(gc_capacity(&v) == 1) << "Vector reports incorrect capacity.";
	gc_vector_uinit(&v);
}

TEST(test_capacity, reserve)
{
	gc_vector v;
	gc_vector_init(&v, sizeof(int));
	v.reserve(&v, num_of_multiple_elems);
    ASSERT_TRUE(num_of_multiple_elems <= (long)gc_capacity(&v)) <<
        "Vector reports incorrect capacity.";
	gc_vector_uinit(&v);
}



TEST(test_capacity, ratio_vector)
{
	gc_vector v;
	gc_vector_init_allocator(&v, gc_default_allocator(sizeof(int)), 3.0);
    ASSERT_TRUE(gc_capacity_ratio(&v) == 3.0) <<
        "Vector reports incorrect capacity ratio.";
	gc_vector_uinit(&v);
}



#define test_count(ctnr, nelems, pfn) \
	do { \
		int i; \
		ctnr c; \
		ctnr##_init(&c, sizeof(i)); \
		for (i = 0; i < nelems; ++i) \
			c.pfn(&c, &i); \
		ASSERT_TRUE(gc_count(&c) == (size_t)nelems) << \
            "Incorrect number of elements reported."; \
		ctnr##_uinit(&c); \
    } while (false)

TEST(test_count, list_empty)
{
	test_count(gc_list, 0, push_back);
}

TEST(test_count, queue_empty)
{
	test_count(gc_queue, 0, push_back);
}

TEST(test_count, slist_empty)
{
	test_count(gc_slist, 0, push_front);
}

TEST(test_count, stack_empty)
{
	test_count(gc_stack, 0, push_front);
}

TEST(test_count, vector_empty)
{
	test_count(gc_vector, 0, push_back);
}

TEST(test_count, list_single_element)
{
	test_count(gc_list, 1, push_back);
}

TEST(test_count, queue_single_element)
{
	test_count(gc_queue, 1, push_back);
}

TEST(test_count, slist_single_element)
{
	test_count(gc_slist, 1, push_front);
}

TEST(test_count, stack_single_element)
{
	test_count(gc_stack, 1, push_front);
}

TEST(test_count, vector_single_element)
{
	test_count(gc_vector, 1, push_back);
}

TEST(test_count, list_multiple_elements)
{
	test_count(gc_list, num_of_multiple_elems, push_back);
}

TEST(test_count, queue_multiple_elements)
{
	test_count(gc_queue, num_of_multiple_elems, push_back);
}

TEST(test_count, slist_multiple_elements)
{
	test_count(gc_slist, num_of_multiple_elems, push_front);
}

TEST(test_count, stack_multiple_elements)
{
	test_count(gc_stack, num_of_multiple_elems, push_front);
}

TEST(test_count, vector_multiple_elements)
{
	test_count(gc_vector, num_of_multiple_elems, push_back);
}



#define test_empty_empty(ctnr) \
	do { \
		ctnr c; \
		ctnr##_init(&c, sizeof(int)); \
		ASSERT_TRUE(gc_empty(&c) == _true) << \
            "Container is not empty when it should be."; \
		ctnr##_uinit(&c); \
    } while (false)

TEST(test_empty, list_empty)
{
	test_empty_empty(gc_list);
}

TEST(test_empty, queue_empty)
{
	test_empty_empty(gc_queue);
}

TEST(test_empty, slist_empty)
{
	test_empty_empty(gc_slist);
}

TEST(test_empty, stack_empty)
{
	test_empty_empty(gc_stack);
}

TEST(test_empty, vector_empty)
{
	test_empty_empty(gc_vector);
}



#define test_empty_not_empty(ctnr, pfn) \
	do { \
		int i = 0; \
		ctnr c; \
		ctnr##_init(&c, sizeof(i)); \
		c.pfn(&c, &i); \
		ASSERT_TRUE(gc_empty(&c) == _false) << \
            "Container is reported as empty when it is not."; \
		ctnr##_uinit(&c); \
    } while (false)

TEST(test_empty, list_not_empty)
{
	test_empty_not_empty(gc_list, push_back);
}

TEST(test_empty, queue_not_empty)
{
	test_empty_not_empty(gc_queue, push_back);
}

TEST(test_empty, slist_not_empty)
{
	test_empty_not_empty(gc_slist, push_front);
}

TEST(test_empty, stack_not_empty)
{
	test_empty_not_empty(gc_stack, push_front);
}

TEST(test_empty, vector_not_empty)
{
	test_empty_not_empty(gc_vector, push_back);
}
