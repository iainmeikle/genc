#ifndef TEST_CONTAINER_RESIZE_COMMON_H
#define TEST_CONTAINER_RESIZE_COMMON_H

#include <stddef.h>
#include <gtest/gtest.h>
#include <genc/iterator.h>

extern void (*test_container_init)(test_container *, size_t);
extern void (*test_container_uinit)(test_container *);
extern const int num_of_multiple_elems;

void test_resize_increase_common()
{
	int dv = 1;
	test_container c;
	test_container_iterator cb, ce;
	gc_iterator *b, * e;
	test_container_init(&c, sizeof(int));
	c.resize(&c, num_of_multiple_elems, &dv);
	cb = c.begin(&c);
	ce = c.end(&c);
	e = (gc_iterator *)&ce;

	for (b = (gc_iterator *)&cb; b->it != e->it; gc_advance(&cb), b = (gc_iterator *)&cb)
	{
                ASSERT_TRUE(*((int *)b->value) == dv) << "Unexpected value found in stack.";
	}

	test_container_uinit(&c);
}

#endif
