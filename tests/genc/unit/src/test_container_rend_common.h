#ifndef TEST_CONTAINER_REND_COMMON_H
#define TEST_CONTAINER_REND_COMMON_H

#include <stddef.h>
#include <gtest/gtest.h>
#include <genc/iterator.h>

extern void (*test_container_init)(test_container *, size_t);
extern void (*test_container_uinit)(test_container *);
extern const int num_of_multiple_elems;

void test_rend_empty_common()
{
	test_container c;
	test_container_iterator cre;
	gc_iterator *re;
	test_container_init(&c, sizeof(int));
	cre = c.rend(&c);
	re = (gc_iterator *)&cre;
        ASSERT_TRUE(re->it == 0) << "End iterator incorrect.";
        ASSERT_TRUE(re->value == 0) << "End iterator incorrectly references a value.";
	test_container_uinit(&c);
}

#endif
