#include <gtest/gtest.h>
#include <genc/allocator.h>

TEST(test_allocate, gc_default_allocator)
{
	size_t elem_size = sizeof(double);
	gc_allocator a = gc_default_allocator(elem_size);
	void *am = NULL;
	am = gc_allocate(&a, a.elem_size, 1, am);
    ASSERT_TRUE(am != NULL) << "Memory not allocated.";
	gc_deallocate(&a, am);
}

TEST(test_elem_size, gc_default_allocator)
{
	size_t elem_size = sizeof(double);
	gc_allocator a = gc_default_allocator(elem_size);
    ASSERT_TRUE(elem_size == a.elem_size) <<
        "Allocator element size incorrectly set.";
}
