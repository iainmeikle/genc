#include <gtest/gtest.h>
#include <genc/vector.h>

typedef gc_vector test_container;
typedef gc_vector_iterator test_container_iterator;

#include "test_container_back_push_back_common.h"
#include "test_container_begin_common.h"
#include "test_container_begin_push_back_common.h"
#include "test_container_end_common.h"
#include "test_container_end_push_back_common.h"
#include "test_container_clear_push_back_common.h"
#include "test_container_erase_common.h"
#include "test_container_front_push_front_common.h"
#include "test_container_insert_after_common.h"
#include "test_container_insert_before_common.h"
#include "test_container_pop_back_common.h"
#include "test_container_pop_front_push_back_common.h"
#include "test_container_push_back_common.h"
#include "test_container_push_front_common.h"
#include "test_container_rbegin_common.h"
#include "test_container_rbegin_push_back_common.h"
#include "test_container_rend_common.h"
#include "test_container_rend_push_back_common.h"
#include "test_container_reserve_common.h"
#include "test_container_reserve_push_back_common.h"
#include "test_container_resize_common.h"
#include "test_container_resize_push_back_common.h"

void (*test_container_init)(test_container *, size_t) = &gc_vector_init;
void (*test_container_uinit)(test_container *) = &gc_vector_uinit;

const int num_of_multiple_elems = 100;



TEST(test_at, single_element)
{
	int i = 1, a = 0;
	gc_vector v;
	gc_vector_init(&v, sizeof(i));
	v.push_back(&v, &i);
	a = *(int *)v.at(&v, 0);
	ASSERT_TRUE(a == i) << "Vector at function returns incorrect value.";
	gc_vector_uinit(&v);
}

TEST(test_at, two_elements)
{
	int i = 1, j = 2, af = 0, al = 0;
	gc_vector v;
	gc_vector_init(&v, sizeof(i));
	v.push_back(&v, &i);
	v.push_back(&v, &j);
	af = *(int *)v.at(&v, 0);
	al = *(int *)v.at(&v, 1);
	ASSERT_TRUE(af == i) << "Vector at function returns incorrect value.";
	ASSERT_TRUE(al == j) << "Vector at function returns incorrect value.";
	gc_vector_uinit(&v);
}

TEST(test_at, multiple_elements)
{
	int i;
	gc_vector v;
	gc_vector_init(&v, sizeof(i));

	for (i = 0; i < num_of_multiple_elems; ++i)
	{
		v.push_back(&v, &i);
	}

	for (i = 0; i < num_of_multiple_elems; ++i)
	{
		ASSERT_TRUE(i == *(int *)v.at(&v, i)) <<
			"Vector at function returns incorrect value.";
	}

	gc_vector_uinit(&v);
}



TEST(test_back, single_element)
{
	test_back_single_element_push_back_common();
}

TEST(test_back, multiple_elements)
{
	test_back_multiple_elements_push_back_common();
}



TEST(test_begin, empty)
{
	test_begin_empty_common();
}

TEST(test_begin, not_empty)
{
	test_begin_not_empty_push_back_common();
}



TEST(test_clear, empty)
{
	test_clear_empty_push_back_common();
}

TEST(test_clear, single_element)
{
	test_clear_single_element_push_back_common();
}

TEST(test_clear, multiple_elements)
{
	test_clear_multiple_elements_push_back_common();
}



TEST(test_end, empty)
{
	test_end_empty_common();
}

TEST(test_end, not_empty)
{
	test_end_not_empty_push_back_common();
}



TEST(test_erase, all_empty)
{
	test_erase_all_empty_common();
}

TEST(test_erase, all_single_element)
{
	test_erase_all_single_element_common();
}

TEST(test_erase, all_multiple_elements)
{
	test_erase_all_multiple_elements_common();
}

TEST(test_erase, first_single_element_single_element)
{
	test_erase_first_single_element_single_element_common();
}

TEST(test_erase, last_single_element_single_element)
{
	test_erase_last_single_element_single_element_common();
}

TEST(test_erase, first_single_element_multiple_elements)
{
	test_erase_first_single_element_multiple_elements_common();
}

TEST(test_erase, last_single_element_multiple_elements)
{
	test_erase_last_single_element_multiple_elements_common();
}

TEST(test_erase, multiple_elements_start_multiple_elements)
{
	test_erase_multiple_elements_start_multiple_elements_common();
}

TEST(test_erase, multiple_elements_end_multiple_elements)
{
	test_erase_multiple_elements_end_multiple_elements_common();
}



TEST(test_front, single_element)
{
	test_front_single_element_push_front_common();
}

TEST(test_front, multiple_elements)
{
	test_front_multiple_elements_push_front_common();
}



TEST(test_insert, after_begin_single_element)
{
	test_insert_after_begin_single_element_common();
}

TEST(test_insert, after_begin_multiple_elements)
{
	test_insert_after_begin_multiple_elements_common();
}

TEST(test_insert, after_rbegin_single_element)
{
	test_insert_after_rbegin_single_element_common();
}

TEST(test_insert, after_rbegin_multiple_elements)
{
	test_insert_after_rbegin_multiple_elements_common();
}

TEST(test_insert, after_rend_single_element)
{
	test_insert_after_rend_single_element_common();
}

TEST(test_insert, after_rend_multiple_elements)
{
	test_insert_after_rend_multiple_elements_common();
}



TEST(test_insert, before_begin_single_element)
{
	test_insert_before_begin_single_element_common();
}

TEST(test_insert, before_begin_multiple_elements)
{
	test_insert_before_begin_multiple_elements_common();
}

TEST(test_insert, before_end_single_element)
{
	test_insert_before_end_single_element_common();
}

TEST(test_insert, before_end_multiple_elements)
{
	test_insert_before_end_multiple_elements_common();
}

TEST(test_insert, before_rbegin_single_element)
{
	test_insert_before_rbegin_single_element_common();
}

TEST(test_insert, before_rbegin_multiple_elements)
{
	test_insert_before_rbegin_multiple_elements_common();
}



TEST(test_pop_back, single_element)
{
	test_pop_back_single_element_common();
}

TEST(test_pop_back, multiple_elements)
{
	test_pop_back_multiple_elements_common();
}



TEST(test_pop_front, single_element)
{
	test_pop_front_single_element_push_back_common();
}

TEST(test_pop_front, multiple_elements)
{
	test_pop_front_multiple_elements_push_back_common();
}



TEST(test_push_back, empty)
{
	test_push_back_empty_common();
}

TEST(test_push_back, single_element)
{
	test_push_back_single_element_common();
}



TEST(test_push_front, empty)
{
	test_push_front_empty_common();
}

TEST(test_push_front, single_element)
{
	test_push_front_single_element_common();
}



TEST(test_rbegin, empty)
{
	test_rbegin_empty_common();
}

TEST(test_rbegin, not_empty)
{
	test_rbegin_not_empty_common();
}



TEST(test_rend, empty)
{
	test_rend_empty_common();
}

TEST(test_rend, not_empty)
{
	test_rend_not_empty_push_back_common();
}



TEST(test_reserve, decrease)
{
	test_reserve_decrease_push_back_common();
}

TEST(test_reserve, increase)
{
	test_reserve_increase_common();
}



TEST(test_resize, decrease)
{
	test_resize_decrease_push_back_common();
}

TEST(test_resize, increase)
{
	test_resize_increase_common();
}
