#ifndef TEST_CONTAINER_INSERT_AFTER_COMMON_H
#define TEST_CONTAINER_INSERT_AFTER_COMMON_H

#include <stddef.h>
#include <gtest/gtest.h>
#include <genc/iterator.h>

extern void (*test_container_init)(test_container *, size_t);
extern void (*test_container_uinit)(test_container *);
extern const int num_of_multiple_elems;

static void test_insert_after_begin(size_t advances, size_t nelems)
{
	size_t first = 0, i;
	size_t a;
	test_container c;
	test_container_iterator cb, ci;
	test_container_init(&c, sizeof(size_t));

	for (i = first; i < nelems; ++i)
	{
		c.push_back(&c, &i);
	}

	cb = c.begin(&c);
	c.insert_after(&c, &cb, &i);
	ci = c.begin(&c);

	for (a = 0; a < advances; ++a)
	{
		gc_advance(&ci);
	}

		ASSERT_TRUE(gc_count(&c) == nelems + 1) <<
            "Incorrect number of elements in container.";
        ASSERT_TRUE(*(size_t *)(((gc_iterator *)&ci)->value) == i) <<
            "Incorrect value reported at insertion point.";
	test_container_uinit(&c);
}

void test_insert_after_begin_single_element_common()
{
	test_insert_after_begin(1, 1);
}

void test_insert_after_begin_multiple_elements_common()
{
	test_insert_after_begin(1, num_of_multiple_elems);
}

static void test_insert_after_rbegin(size_t radvances, size_t nelems)
{
	size_t first = 0, i;
	size_t ra;
	test_container c;
	test_container_iterator cb, ci;
	test_container_init(&c, sizeof(size_t));

	for (i = first; i < nelems; ++i)
	{
		c.push_back(&c, &i);
	}

	cb = c.rbegin(&c);
	c.insert_after(&c, &cb, &i);
	ci = c.rbegin(&c);

	for (ra = 0; ra < radvances; ++ra)
	{
		gc_radvance(&ci);
	}

		ASSERT_TRUE(gc_count(&c) == nelems + 1) <<
            "Incorrect number of elements in container.";
        ASSERT_TRUE(*(size_t *)(((gc_iterator *)&ci)->value) == i) <<
            "Incorrect value reported at insertion point.";
	test_container_uinit(&c);
}

void test_insert_after_rbegin_single_element_common()
{
	test_insert_after_rbegin(0, 1);
}

void test_insert_after_rbegin_multiple_elements_common()
{
	test_insert_after_rbegin(0, num_of_multiple_elems);
}

static void test_insert_after_rend(size_t nelems)
{
	size_t first = 0, i;
	test_container c;
	test_container_iterator cb, ci;
	test_container_init(&c, sizeof(size_t));

	for (i = first; i < nelems; ++i)
	{
		c.push_back(&c, &i);
	}

	cb = c.rend(&c);
	c.insert_after(&c, &cb, &i);
	ci = c.begin(&c);
		ASSERT_TRUE(gc_count(&c) == nelems + 1) <<
            "Incorrect number of elements in container.";
        ASSERT_TRUE(*(size_t *)(((gc_iterator *)&ci)->value) == i) <<
            "Incorrect value reported at insertion point.";
	test_container_uinit(&c);
}

void test_insert_after_rend_single_element_common()
{
	test_insert_after_rend(1);
}

void test_insert_after_rend_multiple_elements_common()
{
	test_insert_after_rend(num_of_multiple_elems);
}

#endif
