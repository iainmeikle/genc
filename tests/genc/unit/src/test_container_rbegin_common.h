#ifndef TEST_CONTAINER_RBEGIN_COMMON_H
#define TEST_CONTAINER_RBEGIN_COMMON_H

#include <stddef.h>
#include <gtest/gtest.h>
#include <genc/iterator.h>

extern void (*test_container_init)(test_container *, size_t);
extern void (*test_container_uinit)(test_container *);
extern const int num_of_multiple_elems;

void test_rbegin_empty_common()
{
	test_container c;
	test_container_iterator crb, cre;
	gc_iterator *b, * e;
	test_container_init(&c, sizeof(int));
	crb = c.rbegin(&c);
	cre = c.rend(&c);
	b = (gc_iterator *)&crb;
	e = (gc_iterator *)&cre;
        ASSERT_TRUE(b->it == e->it) << "Begin iterator is not equal to end iterator.";
        ASSERT_TRUE(b->value == e->value) << "Begin iterator is not equal to end iterator.";
	test_container_uinit(&c);
}

#endif
