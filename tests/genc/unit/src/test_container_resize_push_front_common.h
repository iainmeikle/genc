#ifndef TEST_CONTAINER_RESIZE_PUSH_FRONT_COMMON_H
#define TEST_CONTAINER_RESIZE_PUSH_FRONT_COMMON_H

#include <stddef.h>
#include <gtest/gtest.h>
#include <genc/iterator.h>

extern void (*test_container_init)(test_container *, size_t);
extern void (*test_container_uinit)(test_container *);
extern const int num_of_multiple_elems;

void test_resize_decrease_push_front_common()
{
	int i, dv = 1, hs = (num_of_multiple_elems / 2);
	test_container c;
	test_container_iterator cb, ce;
	gc_iterator *b, * e;
	test_container_init(&c, sizeof(int));

	for (i = 0; i < num_of_multiple_elems; ++i)
	{
		c.push_front(&c, &i);
	}

	c.resize(&c, hs, &dv);
		ASSERT_TRUE(gc_count(&c) == (size_t)hs) << "Queue resized incorrectly.";
	cb = c.begin(&c);
	ce = c.end(&c);
	e = (gc_iterator *)&ce;

	for (b = (gc_iterator *)&cb; b->it != e->it; gc_advance(&cb), b = (gc_iterator *)&cb)
	{
                ASSERT_TRUE(*((int *)b->value) <= hs) <<
                    "Unexpected value found in queue.";
	}

	test_container_uinit(&c);
}

#endif
