#ifndef TEST_CONTAINER_POP_FRONT_PUSH_FRONT_COMMON_H
#define TEST_CONTAINER_POP_FRONT_PUSH_FRONT_COMMON_H

#include <stddef.h>
#include <gtest/gtest.h>
#include <genc/iterator.h>

extern void (*test_container_init)(test_container *, size_t);
extern void (*test_container_uinit)(test_container *);
extern const int num_of_multiple_elems;

void test_pop_front_single_element_common()
{
	int i = 1;
	test_container c;
	test_container_iterator cb;
	test_container_init(&c, sizeof(int));
	c.push_front(&c, &i);
	c.pop_front(&c);
	cb = c.begin(&c);
        ASSERT_TRUE(((gc_iterator *)&cb)->it == 0) <<
            "Begin iterator is not null.";
        ASSERT_TRUE(((gc_iterator *)&cb)->value == 0) <<
            "Begin iterator value is not null.";
	test_container_uinit(&c);
}

void test_pop_front_multiple_elements_common()
{
	int i = 1;
	test_container c;
	test_container_iterator cb;
	test_container_init(&c, sizeof(int));
	c.push_front(&c, &i);
	++i;
	c.push_front(&c, &i);
	c.pop_front(&c);
	cb = c.begin(&c);
        ASSERT_TRUE(*((int *)((gc_iterator *)&cb)->value) == (i - 1)) <<
            "Begin iterator value incorrect.";
	test_container_uinit(&c);
}

#endif
