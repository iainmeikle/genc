#ifndef TEST_CONTAINER_BEGIN_COMMON_H
#define TEST_CONTAINER_BEGIN_COMMON_H

#include <stddef.h>
#include <gtest/gtest.h>
#include <genc/iterator.h>

extern void (*test_container_init)(test_container *, size_t);
extern void (*test_container_uinit)(test_container *);
extern const int num_of_multiple_elems;

void test_begin_empty_common()
{
	test_container c;
	test_container_iterator cb, ce;
	gc_iterator *b, * e;
	test_container_init(&c, sizeof(int));
	cb = c.begin(&c);
	ce = c.end(&c);
	b = (gc_iterator *)&cb;
	e = (gc_iterator *)&ce;
        ASSERT_TRUE(b->it == e->it) <<
            "Begin iterator is not equal to end iterator.";
        ASSERT_TRUE(b->value == e->value) <<
            "Begin iterator is not equal to end iterator.";
	test_container_uinit(&c);
}

#endif
