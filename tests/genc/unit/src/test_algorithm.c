#include <stddef.h>
#include <gtest/gtest.h>
#include <genc/iterator.h>
#include <genc/container.h>
#include <genc/list.h>
#include <genc/queue.h>
#include <genc/slist.h>
#include <genc/stack.h>
#include <genc/vector.h>
#include <genc/algorithm.h>



static const int num_of_multiple_elems = 100;



void accumulate_func(gc_iterator *it, void *v)
{
	*((int *)v) += *((int *)it->value);
}

#define test_accumulate_empty(ctnr, bfn, efn, accfn) \
	do { \
		ctnr c; \
		ctnr##_iterator bi, ei; \
		int acc = 0; \
		ctnr##_init(&c, sizeof(acc)); \
		bi = c.bfn(&c); \
		ei = c.efn(&c); \
		gc_accumulate((gc_forward_iterator*)&bi, (gc_forward_iterator*)&ei, &acc, &accfn); \
		ASSERT_TRUE(acc == 0) << "Incorrectly accumulated value."; \
		ctnr##_uinit(&c); \
    } while (false)

TEST(test_accumulate, forward_empty_list)
{
	test_accumulate_empty(gc_list, begin, end, accumulate_func);
}

TEST(test_accumulate, forward_empty_queue)
{
	test_accumulate_empty(gc_queue, begin, end, accumulate_func);
}

TEST(test_accumulate, forward_empty_slist)
{
	test_accumulate_empty(gc_slist, begin, end, accumulate_func);
}

TEST(test_accumulate, forward_empty_stack)
{
	test_accumulate_empty(gc_stack, begin, end, accumulate_func);
}

TEST(test_accumulate, forward_empty_vector)
{
	test_accumulate_empty(gc_vector, begin, end, accumulate_func);
}

TEST(test_accumulate, reverse_empty_list)
{
	test_accumulate_empty(gc_list, rbegin, rend, accumulate_func);
}

TEST(test_accumulate, reverse_empty_vector)
{
	test_accumulate_empty(gc_vector, rbegin, rend, accumulate_func);
}



#define test_accumulate_single_element(ctnr, bfn, efn, accfn, pushfn) \
	do { \
		ctnr c; \
		ctnr##_iterator bi, ei; \
		int acc = 0, i = 1; \
		ctnr##_init(&c, sizeof(i)); \
		c.pushfn(&c, &i); \
		bi = c.bfn(&c); \
		ei = c.efn(&c); \
		gc_accumulate((gc_forward_iterator*)&bi, (gc_forward_iterator*)&ei, &acc, &accfn); \
		ASSERT_TRUE(acc == i) << "Incorrectly accumulated value."; \
		ctnr##_uinit(&c); \
    } while (false)

TEST(test_accumulate, forward_single_element_list)
{
	test_accumulate_single_element(gc_list, begin, end, accumulate_func, push_back);
}

TEST(test_accumulate, forward_single_element_queue)
{
	test_accumulate_single_element(gc_queue, begin, end, accumulate_func, push_back);
}

TEST(test_accumulate, forward_single_element_slist)
{
	test_accumulate_single_element(gc_slist, begin, end, accumulate_func, push_front);
}

TEST(test_accumulate, forward_single_element_stack)
{
	test_accumulate_single_element(gc_stack, begin, end, accumulate_func, push_front);
}

TEST(test_accumulate, forward_single_element_vector)
{
	test_accumulate_single_element(gc_vector, begin, end, accumulate_func, push_back);
}

TEST(test_accumulate, reverse_single_element_list)
{
	test_accumulate_single_element(gc_list, rbegin, rend, accumulate_func, push_back);
}

TEST(test_accumulate, reverse_single_element_vector)
{
	test_accumulate_single_element(gc_vector, rbegin, rend, accumulate_func, push_back);
}



#define test_accumulate_multiple_elements(ctnr, bfn, efn, accfn, pushfn) \
	do { \
		ctnr c; \
		ctnr##_iterator bi, ei; \
		int acc = 0, i; \
		ctnr##_init(&c, sizeof(i)); \
		for (i = 1; i <= num_of_multiple_elems; ++i) \
			c.pushfn(&c, &i); \
		bi = c.bfn(&c); \
		ei = c.efn(&c); \
		gc_accumulate((gc_forward_iterator*)&bi, (gc_forward_iterator*)&ei, &acc, &accfn); \
        ASSERT_TRUE( \
            acc == ((num_of_multiple_elems * (num_of_multiple_elems + 1))) / 2) << \
			"Incorrectly accumulated value."; \
		ctnr##_uinit(&c); \
    } while (false)

TEST(test_accumulate, forward_multiple_elements_list)
{
	test_accumulate_multiple_elements(gc_list, begin, end, accumulate_func, push_back);
}

TEST(test_accumulate, forward_multiple_elements_queue)
{
	test_accumulate_multiple_elements(gc_queue, begin, end, accumulate_func, push_back);
}

TEST(test_accumulate, forward_multiple_elements_slist)
{
	test_accumulate_multiple_elements(gc_slist, begin, end, accumulate_func, push_front);
}

TEST(test_accumulate, forward_multiple_elements_stack)
{
	test_accumulate_multiple_elements(gc_stack, begin, end, accumulate_func, push_front);
}

TEST(test_accumulate, forward_multiple_elements_vector)
{
	test_accumulate_multiple_elements(gc_vector, begin, end, accumulate_func, push_back);
}

TEST(test_accumulate, reverse_multiple_elements_list)
{
	test_accumulate_multiple_elements(gc_list, rbegin, rend, accumulate_func, push_back);
}

TEST(test_accumulate, reverse_multiple_elements_vector)
{
	test_accumulate_multiple_elements(gc_vector, rbegin, rend, accumulate_func, push_back);
}



#define test_copy_empty(ctnr, bfn, efn) \
	do { \
		ctnr s, d; \
		ctnr##_iterator bi, ei, oi; \
		ctnr##_init(&s, sizeof(int)); \
		ctnr##_init(&d, sizeof(int)); \
		bi = s.bfn(&s); \
		ei = s.efn(&s); \
		oi = d.bfn(&d); \
		gc_copy((gc_forward_iterator*)&bi, (gc_forward_iterator*)&ei, (gc_forward_iterator*)&oi); \
		ASSERT_TRUE(gc_count(&s) == 0U) << "Source container is not empty."; \
		ASSERT_TRUE(gc_count(&d) == 0U) << "Destination container is not empty."; \
		ctnr##_uinit(&s); \
		ctnr##_uinit(&d); \
    } while (false)

TEST(test_copy, forward_empty_list)
{
	test_copy_empty(gc_list, begin, end);
}

TEST(test_copy, forward_empty_queue)
{
	test_copy_empty(gc_queue, begin, end);
}

TEST(test_copy, forward_empty_slist)
{
	test_copy_empty(gc_slist, begin, end);
}

TEST(test_copy, forward_empty_stack)
{
	test_copy_empty(gc_stack, begin, end);
}

TEST(test_copy, forward_empty_vector)
{
	test_copy_empty(gc_vector, begin, end);
}

TEST(test_copy, reverse_empty_list)
{
	test_copy_empty(gc_list, rbegin, rend);
}

TEST(test_copy, reverse_empty_vector)
{
	test_copy_empty(gc_vector, rbegin, rend);
}



#define test_copy(ctnr, bfn, efn, pushfn, nelems) \
	do { \
		int i; \
		ctnr s, d; \
		ctnr##_iterator bi, ei, obi, oei; \
		ctnr##_init(&s, sizeof(i)); \
		ctnr##_init(&d, sizeof(i)); \
		for (i = 1; i <= nelems; ++i) \
			s.pushfn(&s, &i); \
		i = 0; \
		d.resize(&d, gc_count(&s), &i); \
		bi = s.bfn(&s); \
		ei = s.efn(&s); \
		obi = d.bfn(&d); \
		gc_copy((gc_forward_iterator*)&bi, (gc_forward_iterator*)&ei, (gc_forward_iterator*)&obi); \
		ASSERT_TRUE(gc_count(&s) == (size_t)nelems) << "Source container is not empty."; \
		ASSERT_TRUE(gc_count(&d) == (size_t)nelems) << "Destination container is not empty."; \
		for (bi = s.bfn(&s), \
			 obi = d.bfn(&d), \
			 oei = d.efn(&d); \
			 ((gc_iterator*)&bi)->it != ((gc_iterator*)&ei)->it && \
			 ((gc_iterator*)&obi)->it != ((gc_iterator*)&oei)->it; \
			 gc_advance(&bi), \
			 gc_advance(&obi)) \
		{ \
            ASSERT_TRUE( \
				*((int*)((gc_iterator*)&bi)->value) == *((int*)((gc_iterator*)&obi)->value)) << \
                "Values not equal between containers."; \
		} \
		ctnr##_uinit(&s); \
		ctnr##_uinit(&d); \
    } while (false)

TEST(test_copy, forward_single_element_list)
{
	test_copy(gc_list, begin, end, push_back, 1);
}

TEST(test_copy, forward_single_element_queue)
{
	test_copy(gc_queue, begin, end, push_back, 1);
}

TEST(test_copy, forward_single_element_slist)
{
	test_copy(gc_slist, begin, end, push_front, 1);
}

TEST(test_copy, forward_single_element_stack)
{
	test_copy(gc_stack, begin, end, push_front, 1);
}

TEST(test_copy, forward_single_element_vector)
{
	test_copy(gc_vector, begin, end, push_back, 1);
}

TEST(test_copy, reverse_single_element_list)
{
	test_copy(gc_list, rbegin, rend, push_back, 1);
}

TEST(test_copy, reverse_single_element_vector)
{
	test_copy(gc_vector, rbegin, rend, push_back, 1);
}



TEST(test_copy, forward_multiple_elements_list)
{
	test_copy(gc_list, begin, end, push_back, num_of_multiple_elems);
}

TEST(test_copy, forward_multiple_elements_queue)
{
	test_copy(gc_queue, begin, end, push_back, num_of_multiple_elems);
}

TEST(test_copy, forward_multiple_elements_slist)
{
	test_copy(gc_slist, begin, end, push_front, num_of_multiple_elems);
}

TEST(test_copy, forward_multiple_elements_stack)
{
	test_copy(gc_stack, begin, end, push_front, num_of_multiple_elems);
}

TEST(test_copy, forward_multiple_elements_vector)
{
	test_copy(gc_vector, begin, end, push_back, num_of_multiple_elems);
}

TEST(test_copy, reverse_multiple_elements_list)
{
	test_copy(gc_list, rbegin, rend, push_back, num_of_multiple_elems);
}

TEST(test_copy, reverse_multiple_elements_vector)
{
	test_copy(gc_vector, rbegin, rend, push_back, num_of_multiple_elems);
}



int find_calls = 0;
gc_boolean uncalled_find_func(gc_iterator *it)
{
	EXPECT_TRUE(it != NULL) << "Null iterator passed to find_function.";
	++find_calls;
	return _false;
}

#define test_find_empty(ctnr, bfn, efn, ffn) \
	do { \
		ctnr c; \
		ctnr##_iterator bi, ei; \
		gc_boolean found = _false; \
		ctnr##_init(&c, sizeof(int)); \
		bi = c.bfn(&c); \
		ei = c.efn(&c); \
		found = gc_find((gc_forward_iterator*)&bi, (gc_forward_iterator*)&ei, &ffn); \
        ASSERT_TRUE(found == _false) << "Invalid find result."; \
		ctnr##_uinit(&c); \
    } while (false)

TEST(test_find, forward_empty_list)
{
	test_find_empty(gc_list, begin, end, uncalled_find_func);
}

TEST(test_find, forward_empty_queue)
{
	test_find_empty(gc_queue, begin, end, uncalled_find_func);
}

TEST(test_find, forward_empty_slist)
{
	test_find_empty(gc_slist, begin, end, uncalled_find_func);
}

TEST(test_find, forward_empty_stack)
{
	test_find_empty(gc_stack, begin, end, uncalled_find_func);
}

TEST(test_find, forward_empty_vector)
{
	test_find_empty(gc_vector, begin, end, uncalled_find_func);
}

TEST(test_find, reverse_empty_list)
{
	test_find_empty(gc_list, rbegin, rend, uncalled_find_func);
}

TEST(test_find, reverse_empty_vector)
{
	test_find_empty(gc_vector, rbegin, rend, uncalled_find_func);
}


gc_boolean find_one_func(gc_iterator *it)
{
	EXPECT_TRUE(it != NULL) << "Null iterator passed to find_function.";
	return (*((int *)it->value) == 1 ? _true : _false);
}

#define test_find_single_element_existent(ctnr, bfn, efn, ffn, pushfn) \
	do { \
		ctnr c; \
		ctnr##_iterator bi, ei; \
		gc_boolean found = _false; \
		int i = 1; \
		ctnr##_init(&c, sizeof(i)); \
		c.pushfn(&c, &i); \
		bi = c.bfn(&c); \
		ei = c.efn(&c); \
		found = gc_find((gc_forward_iterator*)&bi, (gc_forward_iterator*)&ei, &ffn); \
        ASSERT_TRUE(found == _true) << "Invalid find result."; \
		ASSERT_TRUE(*((int*)(((gc_iterator*)(&bi))->value)) == i) << "Invalid find iterator."; \
		ctnr##_uinit(&c); \
    } while (false)

TEST(test_find, forward_single_element_existent_list)
{
	test_find_single_element_existent(gc_list, begin, end, find_one_func, push_back);
}

TEST(test_find, forward_single_element_existent_queue)
{
	test_find_single_element_existent(gc_queue, begin, end, find_one_func, push_back);
}

TEST(test_find, forward_single_element_existent_slist)
{
	test_find_single_element_existent(gc_slist, begin, end, find_one_func, push_front);
}

TEST(test_find, forward_single_element_existent_stack)
{
	test_find_single_element_existent(gc_stack, begin, end, find_one_func, push_front);
}

TEST(test_find, forward_single_element_existent_vector)
{
	test_find_single_element_existent(gc_vector, begin, end, find_one_func, push_back);
}

TEST(test_find, reverse_single_element_existent_list)
{
	test_find_single_element_existent(gc_list, rbegin, rend, find_one_func, push_back);
}

TEST(test_find, reverse_single_element_existent_vector)
{
	test_find_single_element_existent(gc_vector, rbegin, rend, find_one_func, push_back);
}



#define test_find_single_element_nonexistent(ctnr, bfn, efn, ffn, pushfn) \
	do { \
		ctnr c; \
		ctnr##_iterator bi, ei; \
		gc_boolean found = _false; \
		int i = 0; \
		ctnr##_init(&c, sizeof(i)); \
		c.pushfn(&c, &i); \
		bi = c.bfn(&c); \
		ei = c.efn(&c); \
		found = gc_find((gc_forward_iterator*)&bi, (gc_forward_iterator*)&ei, &ffn); \
        ASSERT_TRUE(found == _false) << "Invalid find result."; \
		ctnr##_uinit(&c); \
    } while (false)

TEST(test_find, forward_single_element_nonexistent_list)
{
	test_find_single_element_nonexistent(gc_list, begin, end, find_one_func, push_back);
}

TEST(test_find, forward_single_element_nonexistent_queue)
{
	test_find_single_element_nonexistent(gc_queue, begin, end, find_one_func, push_back);
}

TEST(test_find, forward_single_element_nonexistent_slist)
{
	test_find_single_element_nonexistent(gc_slist, begin, end, find_one_func, push_front);
}

TEST(test_find, forward_single_element_nonexistent_stack)
{
	test_find_single_element_nonexistent(gc_stack, begin, end, find_one_func, push_front);
}

TEST(test_find, forward_single_element_nonexistent_vector)
{
	test_find_single_element_nonexistent(gc_vector, begin, end, find_one_func, push_back);
}

TEST(test_find, reverse_single_element_nonexistent_list)
{
	test_find_single_element_nonexistent(gc_list, rbegin, rend, find_one_func, push_back);
}

TEST(test_find, reverse_single_element_nonexistent_vector)
{
	test_find_single_element_nonexistent(gc_vector, rbegin, rend, find_one_func, push_back);
}



gc_boolean find_num_of_multiple_elems_func(gc_iterator *it)
{
	EXPECT_TRUE(it != NULL) << "Null iterator passed to find_function.";
	return (*((int *)it->value) == num_of_multiple_elems ? _true : _false);
}

#define test_find_multiple_elements_existent(ctnr, bfn, efn, ffn, pushfn) \
	do { \
		ctnr c; \
		ctnr##_iterator bi, ei; \
		gc_boolean found = _false; \
		int i; \
		ctnr##_init(&c, sizeof(i)); \
		for (i = 1; i <= num_of_multiple_elems; ++i) \
			c.pushfn(&c, &i); \
		bi = c.bfn(&c); \
		ei = c.efn(&c); \
		found = gc_find((gc_forward_iterator*)&bi, (gc_forward_iterator*)&ei, &ffn); \
        ASSERT_TRUE(found == _true) << "Invalid find result."; \
		ASSERT_TRUE((*((int*)(((gc_iterator*)(&bi))->value))) == num_of_multiple_elems) << "Invalid find iterator."; \
		ctnr##_uinit(&c); \
    } while (false)

TEST(test_find, forward_multiple_elements_existent_list)
{
	test_find_multiple_elements_existent(gc_list, begin, end, find_num_of_multiple_elems_func, push_back);
}

TEST(test_find, forward_multiple_elements_existent_queue)
{
	test_find_multiple_elements_existent(gc_queue, begin, end, find_num_of_multiple_elems_func, push_back);
}

TEST(test_find, forward_multiple_elements_existent_slist)
{
	test_find_multiple_elements_existent(gc_slist, begin, end, find_num_of_multiple_elems_func, push_front);
}

TEST(test_find, forward_multiple_elements_existent_stack)
{
	test_find_multiple_elements_existent(gc_stack, begin, end, find_num_of_multiple_elems_func, push_front);
}

TEST(test_find, forward_multiple_elements_existent_vector)
{
	test_find_multiple_elements_existent(gc_vector, begin, end, find_num_of_multiple_elems_func, push_back);
}

TEST(test_find, reverse_multiple_elements_existent_list)
{
	test_find_multiple_elements_existent(gc_list, rbegin, rend, find_num_of_multiple_elems_func, push_back);
}

TEST(test_find, reverse_multiple_elements_existent_vector)
{
	test_find_multiple_elements_existent(gc_vector, rbegin, rend, find_num_of_multiple_elems_func, push_back);
}



#define test_find_multiple_elements_nonexistent(ctnr, bfn, efn, ffn, pushfn) \
	do { \
		ctnr c; \
		ctnr##_iterator bi, ei; \
		gc_boolean found = _false; \
		int i; \
		ctnr##_init(&c, sizeof(i)); \
		for (i = 0; i < num_of_multiple_elems; ++i) \
			c.pushfn(&c, &i); \
		bi = c.bfn(&c); \
		ei = c.efn(&c); \
		found = gc_find((gc_forward_iterator*)&bi, (gc_forward_iterator*)&ei, &ffn); \
        ASSERT_TRUE(found == _false) << "Invalid find result."; \
		ctnr##_uinit(&c); \
    } while (false)

TEST(test_find, forward_multiple_elements_nonexistent_list)
{
	test_find_multiple_elements_nonexistent(gc_list, begin, end, find_num_of_multiple_elems_func, push_back);
}

TEST(test_find, forward_multiple_elements_nonexistent_queue)
{
	test_find_multiple_elements_nonexistent(gc_queue, begin, end, find_num_of_multiple_elems_func, push_back);
}

TEST(test_find, forward_multiple_elements_nonexistent_slist)
{
	test_find_multiple_elements_nonexistent(gc_slist, begin, end, find_num_of_multiple_elems_func, push_front);
}

TEST(test_find, forward_multiple_elements_nonexistent_stack)
{
	test_find_multiple_elements_nonexistent(gc_stack, begin, end, find_num_of_multiple_elems_func, push_front);
}

TEST(test_find, forward_multiple_elements_nonexistent_vector)
{
	test_find_multiple_elements_nonexistent(gc_vector, begin, end, find_num_of_multiple_elems_func, push_back);
}

TEST(test_find, reverse_multiple_elements_nonexistent_list)
{
	test_find_multiple_elements_nonexistent(gc_list, rbegin, rend, find_num_of_multiple_elems_func, push_back);
}

TEST(test_find, reverse_multiple_elements_nonexistent_vector)
{
	test_find_multiple_elements_nonexistent(gc_vector, rbegin, rend, find_num_of_multiple_elems_func, push_back);
}



size_t for_each_calls = 0;
void for_each_func(gc_iterator *it)
{
    ASSERT_TRUE(it != NULL) << "Null iterator passed to for_each_function.";
	++for_each_calls;
}

void uncalled_for_each_func(gc_iterator *it)
{
    ASSERT_TRUE(it == NULL || it != NULL) << "Called uncalled_for_each_function.";
}



#define test_for_each_empty(ctnr, bfn, efn, fefn) \
	do { \
		ctnr c; \
		ctnr##_iterator bi, ei; \
		ctnr##_init(&c, sizeof(int)); \
		bi = c.bfn(&c); \
		ei = c.efn(&c); \
		gc_for_each((gc_forward_iterator*)&bi, (gc_forward_iterator*)&ei, &fefn); \
		ctnr##_uinit(&c); \
    } while (false)

TEST(test_for_each, forward_empty_list)
{
	test_for_each_empty(gc_list, begin, end, uncalled_for_each_func);
}

TEST(test_for_each, forward_empty_queue)
{
	test_for_each_empty(gc_queue, begin, end, uncalled_for_each_func);
}

TEST(test_for_each, forward_empty_slist)
{
	test_for_each_empty(gc_slist, begin, end, uncalled_for_each_func);
}

TEST(test_for_each, forward_empty_stack)
{
	test_for_each_empty(gc_stack, begin, end, uncalled_for_each_func);
}

TEST(test_for_each, forward_empty_vector)
{
	test_for_each_empty(gc_vector, begin, end, uncalled_for_each_func);
}

TEST(test_for_each, reverse_empty_list)
{
	test_for_each_empty(gc_list, rbegin, rend, uncalled_for_each_func);
}

TEST(test_for_each, reverse_empty_vector)
{
	test_for_each_empty(gc_vector, rbegin, rend, uncalled_for_each_func);
}



#define test_for_each_single_element(ctnr, bfn, efn, fefn, pushfn) \
	do { \
		ctnr c; \
		ctnr##_iterator bi, ei; \
		int i = 1; \
		ctnr##_init(&c, sizeof(i)); \
		c.pushfn(&c, &i); \
		bi = c.bfn(&c); \
		ei = c.efn(&c); \
		for_each_calls = 0; \
		gc_for_each((gc_forward_iterator*)&bi, (gc_forward_iterator*)&ei, &fefn); \
		ASSERT_TRUE(for_each_calls == gc_count(&c)) << "For each called the incorrect number of times"; \
		ctnr##_uinit(&c); \
    } while (false)

TEST(test_for_each, forward_single_element_list)
{
	test_for_each_single_element(gc_list, begin, end, for_each_func, push_back);
}

TEST(test_for_each, forward_single_element_queue)
{
	test_for_each_single_element(gc_queue, begin, end, for_each_func, push_back);
}

TEST(test_for_each, forward_single_element_slist)
{
	test_for_each_single_element(gc_slist, begin, end, for_each_func, push_front);
}

TEST(test_for_each, forward_single_element_stack)
{
	test_for_each_single_element(gc_stack, begin, end, for_each_func, push_front);
}

TEST(test_for_each, forward_single_element_vector)
{
	test_for_each_single_element(gc_vector, begin, end, for_each_func, push_back);
}

TEST(test_for_each, reverse_single_element_list)
{
	test_for_each_single_element(gc_list, rbegin, rend, for_each_func, push_back);
}

TEST(test_for_each, reverse_single_element_vector)
{
	test_for_each_single_element(gc_vector, rbegin, rend, for_each_func, push_back);
}



#define test_for_each_multiple_elements(ctnr, bfn, efn, fefn, pushfn) \
	do { \
		ctnr c; \
		ctnr##_iterator bi, ei; \
		int i; \
		ctnr##_init(&c, sizeof(i)); \
		for (i = 1; i <= num_of_multiple_elems; ++i) \
			c.pushfn(&c, &i); \
		bi = c.bfn(&c); \
		ei = c.efn(&c); \
		for_each_calls = 0; \
		gc_for_each((gc_forward_iterator*)&bi, (gc_forward_iterator*)&ei, &fefn); \
		ASSERT_TRUE(for_each_calls == gc_count(&c)) << "For each called the incorrect number of times"; \
		ctnr##_uinit(&c); \
    } while (false)

TEST(test_for_each, forward_multiple_elements_list)
{
	test_for_each_multiple_elements(gc_list, begin, end, for_each_func, push_back);
}

TEST(test_for_each, forward_multiple_elements_queue)
{
	test_for_each_multiple_elements(gc_queue, begin, end, for_each_func, push_back);
}

TEST(test_for_each, forward_multiple_elements_slist)
{
	test_for_each_multiple_elements(gc_slist, begin, end, for_each_func, push_front);
}

TEST(test_for_each, forward_multiple_elements_stack)
{
	test_for_each_multiple_elements(gc_stack, begin, end, for_each_func, push_front);
}

TEST(test_for_each, forward_multiple_elements_vector)
{
	test_for_each_multiple_elements(gc_vector, begin, end, for_each_func, push_back);
}

TEST(test_for_each, reverse_multiple_elements_list)
{
	test_for_each_multiple_elements(gc_list, rbegin, rend, for_each_func, push_back);
}

TEST(test_for_each, reverse_multiple_elements_vector)
{
	test_for_each_multiple_elements(gc_vector, rbegin, rend, for_each_func, push_back);
}



TEST(test_swap, gc_swap)
{
	int si = 1, di = 2;
	gc_vector s, d;
	gc_vector_iterator sb, db;
	gc_vector_init(&s, sizeof(si));
	gc_vector_init(&d, sizeof(di));
	s.push_back(&s, &si);
	d.push_back(&d, &di);
	sb = s.begin(&s);
	db = d.begin(&d);
    ASSERT_TRUE(*((int *)((gc_iterator *)&sb)->value) == si) <<
        "First element of s not equal to si.";
    ASSERT_TRUE(*((int *)((gc_iterator *)&db)->value) == di) <<
        "First element of d not equal to di";
	gc_swap((gc_iterator *)&sb, (gc_iterator *)&db);
    ASSERT_TRUE(*((int *)((gc_iterator *)&sb)->value) == di) <<
        "First element of s not equal to di.";
    ASSERT_TRUE(*((int *)((gc_iterator *)&db)->value) == si) <<
        "First element of d not equal to si";
	gc_vector_uinit(&s);
	gc_vector_uinit(&d);
}
