#include <stdio.h>
#include <genc/iterator.h>
#include <genc/container.h>
#include <genc/stack.h>
#include <genc/macro.h>

/**
 * Typedef for function pointer to be invoke for every other item.
 */
typedef void (*for_every_other_function)(struct gc_iterator_TAG *);

/**
 * Algorithm to print each item from iterator b to iterator e.
 */
void for_every_other(gc_forward_iterator *b,
					 const gc_forward_iterator *e,
					 for_every_other_function func)
{
	int i = 0;

	/* While the iterator b is not equal to e. */
	while (b->iterator.it != e->iterator.it)
	{
		/* If we are every nth item. */
		if (i % 2)
		{
			func((gc_iterator *)b);
		}

		/* Advance the iterator. */
		gc_advance(b);
		/* Increment i. */
		++i;
	}
}

/**
 * Function to print integer iterator values.
 */
void print_int(gc_iterator *it)
{
	printf("%d\n", *((int *)it->value));
}

int main(void)
{
	/* Allocate stack. */
	gc_stack s;
	int i;
	gc_stack_iterator bsi, esi;
	/* Initialise stack. */
	gc_stack_init(&s, sizeof(i));

	/* Populate the stack with the numbers 100-199 in reverse order. */
	for (i = 100; i < 200; ++i)
	{
		GC_PUSH_FRONT(&s, &i);
	}

	/* Get an iterator to the front/top of the stack. */
	bsi = GC_BEGIN(&s);
	/* Get an iterator to the back/bottom of the stack. */
	esi = GC_END(&s);
	/* Print each item using the given format string. */
	for_every_other((gc_forward_iterator *)&bsi,
					(const gc_forward_iterator *)&esi,
					&print_int);
	/* Uninitialise stack. */
	gc_stack_uinit(&s);
	/* Return success. */
	return 0;
}
