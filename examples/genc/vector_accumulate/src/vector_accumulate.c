#include <stdio.h>
#include <genc/iterator.h>
#include <genc/container.h>
#include <genc/vector.h>
#include <genc/algorithm.h>

/**
 * Function to do the actual accumulation of integer types.
 */
void accumulate_ints(gc_iterator *it, void *v)
{
	*((int *)v) += *((int *)it->value);
}

/**
 * Main function for the vector_accumulate example.
 */
int main(void)
{
	/* Allocate the vector. */
	gc_vector v;
	int i, accumulated;
	gc_vector_iterator bvi, evi;
	/* Initialise the vector. */
	gc_vector_init(&v, sizeof(i));

	/* Populate the vector with the first 1000 natural numbers. */
	for (i = 0; i < 1000; ++i)
	{
		v.push_back(&v, &i);
	}

	/* Get an iterator to the start of the vector. */
	bvi = v.begin(&v);
	/* Get an iterator to the end (one past the last element) of the vector. */
	evi = v.end(&v);
	/* Initialise an integer to store the accumulated values. */
	accumulated = 0;
	/*
	 * Call accumulate passing in the begin and end iterators and the
	 * accumulation function.
	 */
	gc_accumulate((gc_forward_iterator *)&bvi,
			   (const gc_forward_iterator *)&evi,
			   &accumulated,
			   &accumulate_ints);
	/* Print the result. */
	printf("The accumulated value of the first "
		   "1000 natural numbers is %d.\n", accumulated);
	/* Uninitialise vector. */
	gc_vector_uinit(&v);
	/* Return success. */
	return 0;
}
