#include <stdio.h>
#include <genc/iterator.h>
#include <genc/container.h>
#include <genc/list.h>
#include <genc/algorithm.h>
#include <genc/macro.h>

/**
 * Print integer.
 */
void print_int(gc_iterator *it)
{
	printf("%d\n", *((int *)it->value));
}

/**
 * Main function of the list_for_each_macro example.
 */
int main(void)
{
	/* Allocate list. */
	gc_list l;
	int i;
	gc_list_iterator bli, eli;
	/* Initialise list. */
	gc_list_init(&l, sizeof(i));

	/* Populate the list with the first 100 natural numbers in reverse. */
	for (i = 0; i < 100; ++i)
	{
		GC_PUSH_FRONT(&l, &i);
	}

	/* Get an iterator to the start of the list. */
	bli = GC_BEGIN(&l);
	/* Get an iterator to the end (one past the last element) of the list. */
	eli = GC_END(&l);
	/* Call print_int for each integer in the list. */
	gc_for_each((gc_forward_iterator *)&bli,
			 (const gc_forward_iterator *)&eli,
			 &print_int);
	/* Uninitialise list. */
	gc_list_uinit(&l);
	/* Return success. */
	return 0;
}
