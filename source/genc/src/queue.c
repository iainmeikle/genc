/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file list.c
 */

#include <stddef.h>
#include "assert.h"
#include <genc/allocator.h>
#include <genc/iterator.h>
#include <genc/array.h>
#include "array_function.h"
#include <genc/queue.h>

static void *queue_back(const gc_queue *q)
{
	return array_back((const gc_array *)q);
}

static gc_queue_iterator queue_begin(const gc_queue *q)
{
	return array_begin((const gc_array *)q);
}

static gc_queue_iterator queue_end(const gc_queue *q)
{
	return array_end((const gc_array *)q);
}

static void *queue_front(const gc_queue *q)
{
	return array_front((const gc_array *)q);
}

static void queue_pop_front(gc_queue *q)
{
	array_pop_front((gc_array *)q);
}

static void queue_push_back(gc_queue *q, const void *val)
{
	array_push_back((gc_array *)q, val);
}

static void queue_reserve(gc_queue *q, const size_t r)
{
	array_reserve((gc_array *)q, r);
}

static void queue_resize(gc_queue *q, const size_t s, const void *dv)
{
	array_resize((gc_array *)q, s, dv);
}

static void queue_init_common(gc_queue *q)
{
	q->back = &queue_back;
	q->begin = &queue_begin;
	q->end = &queue_end;
	q->front = &queue_front;
	q->pop_front = &queue_pop_front;
	q->push_back = &queue_push_back;
	q->reserve = &queue_reserve;
	q->resize = &queue_resize;
}

void gc_queue_init_allocator(gc_queue *q,
	gc_allocator al,
	double capacity_ratio)
{
	array_init_allocator((gc_array *)q, al, capacity_ratio);
	queue_init_common(q);
}

void gc_queue_init(gc_queue *q, const size_t elem_size)
{
	array_init((gc_array *)q, elem_size);
	queue_init_common(q);
}

void gc_queue_uinit(gc_queue *q)
{
	array_uinit((gc_array *)q);
}
