/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file iterator.c
 */

#include "assert.h"
#include <genc/iterator.h>

void gc_advance(void *vi)
{
	GC_ASSERT(vi != NULL);
	((gc_forward_iterator *)vi)->advance((gc_forward_iterator *)vi);
}

void gc_radvance(void *vi)
{
	GC_ASSERT(vi != NULL);
	((gc_bidirectional_iterator *)vi)->radvance((gc_bidirectional_iterator *)vi);
}

void gc_dadvance(void *vi,
			  const gc_distance_t d)
{
	GC_ASSERT(vi != NULL);
	((gc_random_access_iterator *)vi)->dadvance((gc_random_access_iterator *)vi, d);
}

size_t gc_value_size(const void *vi)
{
	GC_ASSERT(vi != NULL);
	return ((const gc_iterator *)vi)->value_size;
}
