/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file list.c
 */

#include "assert.h"
#include "memory.h"
#include <genc/allocator.h>
#include <genc/iterator.h>
#include <genc/list.h>

static void list_advance_raw(gc_iterator *it)
{
	gc_list_node *ln = NULL;
	GC_ASSERT(it != NULL);
	ln = (gc_list_node *)it->it;

	if (ln->next)
	{
		it->it = (void *)ln->next;
		it->value = (void *)ln->next->value;
	}
	else
	{
		it->it = NULL;
		it->value = NULL;
	}
}

static size_t list_count(const gc_list *l)
{
	return l->container.count;
}

static size_t list_elem_size(const gc_list *l)
{
	return l->container.allocator.elem_size;
}

static gc_list_node *list_create_list_node(gc_list *l, const void *v)
{
	gc_list_node *ln = (gc_list_node *)gc_allocate(
						(gc_allocator *)l, sizeof(gc_list_node), 1, 0);
	GC_ASSERT(l != NULL);
	GC_ASSERT(v != NULL);
	GC_ASSERT(ln != NULL);
	ln->next = NULL;
	ln->previous = NULL;
	ln->value = gc_allocate((gc_allocator *)l, list_elem_size(l), 1, 0);
	GC_ASSERT(ln->value != NULL);
	mem_copy(ln->value, v, list_elem_size(l));
	return ln;
}

static void list_radvance_raw(gc_iterator *it)
{
	gc_list_node *ln = NULL;
	GC_ASSERT(it != NULL);
	ln = (gc_list_node *)it->it;

	if (ln->previous)
	{
		it->it = (void *)ln->previous;
		it->value = (void *)ln->previous->value;
	}
	else
	{
		it->it = NULL;
		it->value = NULL;
	}
}

static void list_advance(void *it)
{
	GC_ASSERT(it != NULL);
	list_advance_raw((gc_iterator *)it);
}

static void list_radvance(void *it)
{
	GC_ASSERT(it != NULL);
	list_radvance_raw((gc_iterator *)it);
}

static gc_list_iterator list_init_iterator_common(const gc_list *l,
		gc_advance_function advance,
		gc_radvance_function radvance)
{
	gc_list_iterator li;
	GC_ASSERT(l != NULL);
	GC_ASSERT(advance != NULL);
	GC_ASSERT(radvance != NULL);
	li.forward_iterator.iterator.it = NULL;
	li.forward_iterator.iterator.value = NULL;
	li.forward_iterator.iterator.value_size = list_elem_size(l);
	li.forward_iterator.advance = advance;
	li.radvance = radvance;
	return li;
}

static gc_list_iterator list_init_iterator(const gc_list *l)
{
	GC_ASSERT(l != NULL);
	return list_init_iterator_common(l, &list_advance, &list_radvance);
}

static gc_list_iterator list_init_riterator(const gc_list *l)
{
	GC_ASSERT(l != NULL);
	return list_init_iterator_common(l, &list_radvance, &list_advance);
}

static void *list_back(const gc_list *l)
{
	GC_ASSERT(l != NULL);
	GC_ASSERT(0 < list_count(l));
	return l->eraw->value;
}

static gc_list_iterator list_begin(const gc_list *l)
{
	GC_ASSERT(l != NULL);

	if (list_count(l))
	{
		gc_list_iterator li = list_init_iterator(l);
		li.forward_iterator.iterator.it = l->braw;
		li.forward_iterator.iterator.value = l->braw->value;
		return li;
	}

	return list_init_iterator(l);
}

static void list_pop_front(gc_list *l);

static void list_clear(gc_list *l)
{
	while (0 < list_count(l))
	{
		list_pop_front(l);
	}
}

static gc_list_iterator list_end(const gc_list *l)
{
	GC_ASSERT(l != NULL);
	return list_init_iterator(l);
}

static void list_erase(gc_list *l, gc_list_iterator *b, const gc_list_iterator *e)
{
	gc_list_node *bln = (gc_list_node *)b->forward_iterator.iterator.it,
			   * eln = (gc_list_node *)e->forward_iterator.iterator.it;
	GC_ASSERT(l != NULL);
	GC_ASSERT(b != NULL);
	GC_ASSERT(e != NULL);

	if (bln == l->braw)
	{
		l->braw = eln;
	}

	if (bln == l->eraw)
	{
		l->eraw = eln;
	}

	for (; b->forward_iterator.iterator.it != e->forward_iterator.iterator.it;
		 bln = (gc_list_node *)b->forward_iterator.iterator.it)
	{
		GC_ASSERT(bln != NULL);

		if (bln->previous)
		{
			bln->previous->next = bln->next;
		}

		if (bln->next)
		{
			bln->next->previous = bln->previous;
		}

		--l->container.count;
		gc_advance(b);
		gc_deallocate((gc_allocator *)l, bln->value);
		gc_deallocate((gc_allocator *)l, bln);
	}
}

static void *list_front(const gc_list *l)
{
	GC_ASSERT(l != NULL);
	GC_ASSERT(0U < list_count(l));
	return l->braw->value;
}

static void list_insert_after_raw(gc_list *l, gc_list_node *marker, const void *v)
{
	gc_list_node *ln = list_create_list_node(l, v);
	GC_ASSERT(l != NULL);
	GC_ASSERT(marker != NULL);
	GC_ASSERT(v != NULL);
	GC_ASSERT(ln != NULL);
	ln->next = marker->next;
	ln->previous = marker;
	marker->next = ln;

	if (ln->next != NULL)
	{
		ln->next->previous = ln;
	}

	if (l->eraw == marker)
	{
		l->eraw = ln;
	}

	++l->container.count;
}

static void list_insert_before_raw(gc_list *l, gc_list_node *marker, const void *v)
{
	gc_list_node *ln = list_create_list_node(l, v);
	GC_ASSERT(l != NULL);
	GC_ASSERT(marker != NULL);
	GC_ASSERT(v != NULL);
	GC_ASSERT(ln != NULL);
	ln->next = marker;
	ln->previous = marker->previous;
	ln->next->previous = ln;

	if (ln->previous != NULL)
	{
		ln->previous->next = ln;
	}

	if (l->braw == marker)
	{
		l->braw = ln;
	}

	++l->container.count;
}

static void list_insert_first_node(gc_list *l, const void *v)
{
	GC_ASSERT(l != NULL);
	GC_ASSERT(v != NULL);
	l->eraw = l->braw = list_create_list_node(l, v);
	GC_ASSERT(l->braw != NULL);
	GC_ASSERT(l->eraw != NULL);
	++l->container.count;
}

static void list_insert_after(gc_list *l, const gc_list_iterator *it, const void *v)
{
	GC_ASSERT(l != NULL);
	GC_ASSERT(it != NULL);
	GC_ASSERT(v != NULL);

	if (it->forward_iterator.iterator.it != NULL)
		list_insert_after_raw(l,
							  (gc_list_node *)it->forward_iterator.iterator.it, v);
	else
	{
		list_insert_before_raw(l, (gc_list_node *)l->braw, v);
	}
}

static void list_insert_before(gc_list *l, const gc_list_iterator *it, const void *v)
{
	GC_ASSERT(l != NULL);
	GC_ASSERT(it != NULL);
	GC_ASSERT(v != NULL);

	if (it->forward_iterator.iterator.it != NULL)
		list_insert_before_raw(l,
							   (gc_list_node *)it->forward_iterator.iterator.it, v);
	else
	{
		list_insert_after_raw(l, l->eraw, v);
	}
}

static void list_pop_back(gc_list *l)
{
	gc_list_node *tmp = NULL;
	GC_ASSERT(l != NULL);
	GC_ASSERT(0U < list_count(l));
	GC_ASSERT(l->eraw != NULL);
	tmp = l->eraw;
	l->eraw = l->eraw->previous;

	if (l->eraw)
	{
		l->eraw->next = NULL;
	}

	--l->container.count;
	gc_deallocate((gc_allocator *)l, tmp->value);
	gc_deallocate((gc_allocator *)l, tmp);
}

static void list_pop_front(gc_list *l)
{
	gc_list_node *tmp = NULL;
	GC_ASSERT(l != NULL);
	GC_ASSERT(0U < list_count(l));
	GC_ASSERT(l->braw != NULL);
	tmp = l->braw;
	l->braw = l->braw->next;

	if (l->braw)
	{
		l->braw->previous = NULL;
	}

	--l->container.count;
	gc_deallocate((gc_allocator *)l, tmp->value);
	gc_deallocate((gc_allocator *)l, tmp);
}

static void list_push_back(gc_list *l, const void *v)
{
	GC_ASSERT(l != NULL);
	GC_ASSERT(v != NULL);

	if (0 < list_count(l))
	{
		list_insert_after_raw(l, l->eraw, v);
	}
	else
	{
		list_insert_first_node(l, v);
	}
}

static void list_push_front(gc_list *l, const void *v)
{
	GC_ASSERT(l != NULL);
	GC_ASSERT(v != NULL);

	if (0 < list_count(l))
	{
		list_insert_before_raw(l, l->braw, v);
	}
	else
	{
		list_insert_first_node(l, v);
	}
}

static gc_list_iterator list_rbegin(const gc_list *l)
{
	GC_ASSERT(l != NULL);

	if (list_count(l))
	{
		gc_list_iterator li = list_init_riterator(l);
		li.forward_iterator.iterator.it = l->eraw;
		li.forward_iterator.iterator.value = l->eraw->value;
		return li;
	}

	return list_init_iterator(l);
}

static gc_list_iterator list_rend(const gc_list *l)
{
	GC_ASSERT(l != NULL);
	return list_init_riterator(l);
}

static void list_resize(gc_list *l, const size_t s, const void *dv)
{
	GC_ASSERT(l != NULL);
	GC_ASSERT(dv != NULL);

	while (list_count(l) < s)
	{
		list_push_back(l, dv);
	}

	while (s < list_count(l))
	{
		list_pop_back(l);
	}
}

void gc_list_init_allocator(gc_list *l, gc_allocator al)
{
	GC_ASSERT(l != NULL);
	l->container.allocator = al;
	l->container.count = 0U;
	l->braw = NULL;
	l->eraw = NULL;
	l->back = &list_back;
	l->begin = &list_begin;
	l->clear = &list_clear;
	l->end = &list_end;
	l->erase = &list_erase;
	l->front = &list_front;
	l->insert_after = &list_insert_after;
	l->insert_before = &list_insert_before;
	l->pop_back = &list_pop_back;
	l->pop_front = &list_pop_front;
	l->push_back = &list_push_back;
	l->push_front = &list_push_front;
	l->rbegin = &list_rbegin;
	l->rend = &list_rend;
	l->resize = &list_resize;
}

void gc_list_init(gc_list *l, const size_t elem_size)
{
	GC_ASSERT(l != NULL);
	GC_ASSERT(elem_size != 0U);
	gc_list_init_allocator(l, gc_default_allocator(elem_size));
}

void gc_list_uinit(gc_list *l)
{
	GC_ASSERT(l != NULL);
	list_clear(l);
}
