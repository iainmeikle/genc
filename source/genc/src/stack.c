/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file stack.c
 */

#include <stddef.h>
#include <genc/allocator.h>
#include <genc/iterator.h>
#include <genc/array.h>
#include "array_function.h"
#include <genc/stack.h>

static gc_stack_iterator stack_begin(const gc_stack *s)
{
	return array_begin((const gc_array *)s);
}

static gc_stack_iterator stack_end(const gc_stack *s)
{
	return array_end((const gc_array *)s);
}

static void *stack_front(const gc_stack *s)
{
	return array_front((const gc_array *)s);
}

static void stack_pop_front(gc_stack *s)
{
	array_pop_front((gc_array *)s);
}

static void stack_push_front(gc_stack *s, const void *val)
{
	array_push_front((gc_array *)s, val);
}

static void stack_reserve(gc_stack *s, const size_t r)
{
	array_reserve((gc_array *)s, r);
}

static void stack_resize(gc_stack *s, const size_t sz, const void *dv)
{
	array_resize((gc_array *)s, sz, dv);
}

static void stack_init_common(gc_stack *s)
{
	s->begin = &stack_begin;
	s->end = &stack_end;
	s->front = &stack_front;
	s->pop_front = &stack_pop_front;
	s->push_front = &stack_push_front;
	s->reserve = &stack_reserve;
	s->resize = &stack_resize;
}

void gc_stack_init_allocator(gc_stack *s,
	gc_allocator al,
	double capacity_ratio)
{
	array_init_allocator((gc_array *)s, al, capacity_ratio);
	stack_init_common(s);
}

void gc_stack_init(gc_stack *s, const size_t elem_size)
{
	array_init((gc_array *)s, elem_size);
	stack_init_common(s);
}

void gc_stack_uinit(gc_stack *s)
{
	array_uinit((gc_array *)s);
}
