#include <stddef.h>
#include <string.h>
#include "memory.h"

void *mem_copy(void *dst, const void *src, const size_t sz)
{
	return memcpy(dst, src, sz);
}

void *mem_move(void *dst, const void *src, const size_t sz)
{
	return memmove(dst, src, sz);
}
