#ifndef EC1F2E77_2151_471F_9548_CCDD881DB98B
#define EC1F2E77_2151_471F_9548_CCDD881DB98B

#include <stddef.h>
#include <genc/allocator.h>
#include <genc/iterator.h>
#include <genc/array.h>

void *array_at(const gc_array * /*array*/, const size_t /*index*/);

void *array_back(const gc_array * /*array*/);

gc_array_iterator array_begin(const gc_array * /*array*/);

void array_clear(gc_array * /*array*/);

gc_array_iterator array_end(const gc_array * /*array*/);

void array_erase(gc_array * /*array*/,
	gc_array_iterator * /*begin*/,
	const gc_array_iterator * /*end*/);

void *array_front(const gc_array * /*array*/);

void array_insert_after(gc_array * /*array*/,
	const gc_array_iterator * /*location*/,
	const void * /*value*/);

void array_insert_before(gc_array * /*array*/,
	const gc_array_iterator * /*location*/,
	const void * /*value*/);

void array_pop_back(gc_array * /*array*/);

void array_pop_front(gc_array * /*array*/);

void array_push_back(gc_array * /*array*/, const void * /*value*/);

void array_push_front(gc_array * /*array*/, const void * /*value*/);

gc_array_iterator array_rbegin(const gc_array * /*array*/);

gc_array_iterator array_rend(const gc_array * /*array*/);

void array_reserve(gc_array * /*array*/, const size_t /*capacity*/);

void array_resize(gc_array * /*array*/,
	const size_t /*size*/,
	const void * /*default value*/);

void array_init_allocator(gc_array * /*array*/,
	gc_allocator /*allocator*/,
	double /*capacity_ratio*/);

void array_init(gc_array * /*array*/, const size_t /*element_size*/);

void array_uinit(gc_array * /*array*/);

#endif
