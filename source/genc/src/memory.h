#ifndef D72B61FB_E7A1_48BD_B64D_E68C4E9BBF9E
#define D72B61FB_E7A1_48BD_B64D_E68C4E9BBF9E

#include <stddef.h>

typedef char mem_unit;

void *mem_copy(void * /*destination*/,
			   const void * /*source*/, const size_t /*size*/);

void *mem_move(void * /*destination*/,
			   const void * /*source*/, const size_t /*size*/);

#endif
