/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file algorithm.c
 */

#include <stdlib.h>
#include <string.h>
#include "assert.h"
#include <genc/boolean.h>
#include <genc/algorithm.h>
#include <genc/iterator.h>

void gc_accumulate(gc_forward_iterator *b,
	const gc_forward_iterator *e,
	void *a,
	const gc_accumulate_function func)
{
    GC_ASSERT(b != NULL);
    GC_ASSERT(e != NULL);
    GC_ASSERT(a != NULL);
    GC_ASSERT(func != NULL);

	for (; b->iterator.it != e->iterator.it; gc_advance(b))
	{
		func(&(b->iterator), a);
	}
}

static void copy_raw(void *dst, const void *src, size_t size)
{
    GC_ASSERT(dst != NULL);
    GC_ASSERT(src != NULL);
    GC_ASSERT(size != 0U);
	memcpy(dst, src, size);
}

void gc_copy(gc_forward_iterator *b,
	const gc_forward_iterator *e,
	gc_forward_iterator *o)
{
	size_t valsz = gc_value_size(b);
    GC_ASSERT(b != NULL);
    GC_ASSERT(e != NULL);
    GC_ASSERT(o != NULL);
    GC_ASSERT(valsz != 0U);

	for (; b->iterator.it != e->iterator.it; gc_advance(b), gc_advance(o))
	{
		copy_raw(o->iterator.value, b->iterator.value, valsz);
	}
}

gc_boolean gc_find(gc_forward_iterator *b,
	const gc_forward_iterator *e,
	const gc_find_function func)
{
    GC_ASSERT(b != NULL);
    GC_ASSERT(e != NULL);
    GC_ASSERT(func != NULL);

	for (; b->iterator.it != e->iterator.it; gc_advance(b))
	{
		if (func(&(b->iterator)))
		{
			return _true;
		}
	}

	return _false;
}

void gc_for_each(gc_forward_iterator *b,
	const gc_forward_iterator *e,
	const gc_for_each_function func)
{
    GC_ASSERT(b != NULL);
    GC_ASSERT(e != NULL);
    GC_ASSERT(func != NULL);

	for (; b->iterator.it != e->iterator.it; gc_advance(b))
	{
		func(&(b->iterator));
	}
}

static void swap_raw(void *v1, void *v2, size_t size)
{
	void *vt = malloc(size);
    GC_ASSERT(v1 != NULL);
    GC_ASSERT(v2 != NULL);
    GC_ASSERT(size != 0U);
    GC_ASSERT(vt != NULL);
	copy_raw(vt, v1, size);
	copy_raw(v1, v2, size);
	copy_raw(v2, vt, size);
	free(vt);
}

void gc_swap(gc_iterator *it1, gc_iterator *it2)
{
    GC_ASSERT(it1 != NULL);
    GC_ASSERT(it1 != NULL);
	GC_ASSERT(gc_value_size(it1) == gc_value_size(it2));
	swap_raw(it1->value, it2->value, gc_value_size(it1));
}
