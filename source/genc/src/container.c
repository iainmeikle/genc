/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file container.c
 */

#include <stddef.h>
#include "assert.h"
#include <genc/boolean.h>
#include <genc/container.h>

size_t gc_capacity(const void *cc)
{
	GC_ASSERT(cc != NULL);
	return ((const gc_capacitive_container *)cc)->capacity;
}

size_t gc_count(const void *c)
{
	GC_ASSERT(c != NULL);
	return ((const gc_container *)c)->count;
}

gc_boolean gc_empty(const void *c)
{
	GC_ASSERT(c != NULL);
	return (gc_count(c) == 0U ? _true : _false);
}

double gc_capacity_ratio(const void *cc)
{
	GC_ASSERT(cc != NULL);
	return ((const gc_capacitive_container *)cc)->capacity_ratio;
}
