/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file vector.c
 */

#include <stdlib.h>
#include <string.h>
#include <genc/allocator.h>
#include <genc/iterator.h>
#include <genc/array.h>
#include "array_function.h"
#include <genc/vector.h>

static void *vector_at(const gc_vector *v, const size_t i)
{
	return array_at((const gc_array *)v, i);
}

static void *vector_back(const gc_vector *v)
{
	return array_back((const gc_array *)v);
}

static gc_vector_iterator vector_begin(const gc_vector *v)
{
	return array_begin((const gc_array *)v);
}

static void vector_clear(gc_vector *v)
{
	array_clear((gc_array *)v);
}

static gc_vector_iterator vector_end(const gc_vector *v)
{
	return array_end((const gc_array *)v);
}

static void vector_erase(gc_vector *v,
	gc_vector_iterator *b,
	const gc_vector_iterator *e)
{
	array_erase((gc_array *)v, b, e);
}

static void *vector_front(const gc_vector *v)
{
	return array_front((const gc_array *)v);
}

static void vector_insert_after(gc_vector *v,
	const gc_vector_iterator *it,
	const void *val)
{
	array_insert_after((gc_array *)v, it, val);
}

static void vector_insert_before(gc_vector *v,
	const gc_vector_iterator *it,
	const void *val)
{
	array_insert_before((gc_array *)v, it, val);
}

static void vector_pop_back(gc_vector *v)
{
	array_pop_back((gc_array *)v);
}

static void vector_pop_front(gc_vector *v)
{
	array_pop_front((gc_array *)v);
}

static void vector_push_back(gc_vector *v, const void *val)
{
	array_push_back((gc_array *)v, val);
}

static void vector_push_front(gc_vector *v, const void *val)
{
	array_push_front((gc_array *)v, val);
}

static gc_vector_iterator vector_rbegin(const gc_vector *v)
{
	return array_rbegin((const gc_array *)v);
}

static gc_vector_iterator vector_rend(const gc_vector *v)
{
	return array_rend((const gc_array *)v);
}

static void vector_reserve(gc_vector *v, const size_t r)
{
	array_reserve((gc_array *)v, r);
}

static void vector_resize(gc_vector *v, const size_t s, const void *dv)
{
	array_resize((gc_array *)v, s, dv);
}

static void vector_init_common(gc_vector *v)
{
	v->at = &vector_at;
	v->back = &vector_back;
	v->begin = &vector_begin;
	v->clear = &vector_clear;
	v->end = &vector_end;
	v->erase = &vector_erase;
	v->front = &vector_front;
	v->insert_after = &vector_insert_after;
	v->insert_before = &vector_insert_before;
	v->pop_back = &vector_pop_back;
	v->pop_front = &vector_pop_front;
	v->push_back = &vector_push_back;
	v->push_front = &vector_push_front;
	v->rbegin = &vector_rbegin;
	v->rend = &vector_rend;
	v->reserve = &vector_reserve;
	v->resize = &vector_resize;
}

void gc_vector_init_allocator(gc_vector *v,
	gc_allocator al,
	double capacity_ratio)
{
	array_init_allocator((gc_array *)v, al, capacity_ratio);
	vector_init_common(v);
}

void gc_vector_init(gc_vector *v, const size_t elem_size)
{
	array_init((gc_array *)v, elem_size);
	vector_init_common(v);
}

void gc_vector_uinit(gc_vector *v)
{
	array_uinit((gc_array *)v);
}
