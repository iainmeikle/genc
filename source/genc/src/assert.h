#ifndef C6AE7FF2_0BB6_46A8_A321_318C14AA5E12
#define C6AE7FF2_0BB6_46A8_A321_318C14AA5E12

#ifndef GENC_DEBUG
#define NDEBUG
#endif

#include <assert.h>

#define GC_ASSERT(expr) assert(expr)

#endif
