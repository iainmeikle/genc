/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file algorithm.c
 */

#include <stdlib.h>
#include "assert.h"
#include <genc/allocator.h>

static void *default_allocate(size_t elem_size,
	size_t num_elems,
	void *mem_ptr)
{
    GC_ASSERT(elem_size != 0U);
	return realloc(mem_ptr, num_elems * elem_size);
}

static void default_deallocate(void *mem_ptr)
{
	free(mem_ptr);
}

gc_allocator gc_default_allocator(const size_t elem_size)
{
	gc_allocator a = { &default_allocate, &default_deallocate, 0 };
    GC_ASSERT(elem_size != 0U);
	a.elem_size = elem_size;
	return a;
}

void *gc_allocate(gc_allocator *a, size_t elem_size,
	size_t num_elems,
	void *mem_ptr)
{
	GC_ASSERT(a != NULL);
    GC_ASSERT(elem_size != 0U);
	return a->allocate(elem_size, num_elems, mem_ptr);
}

void gc_deallocate(gc_allocator *a, void *mem_ptr)
{
	GC_ASSERT(a != NULL);
	a->deallocate(mem_ptr);
}

size_t gc_elem_size(const void *a)
{
	GC_ASSERT(a != NULL);
	return ((const gc_allocator *)a)->elem_size;
}
