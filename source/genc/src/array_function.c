#include <stddef.h>
#include "assert.h"
#include "memory.h"
#include <genc/allocator.h>
#include <genc/iterator.h>
#include <genc/container.h>
#include <genc/array.h>
#include "array_function.h"

static size_t array_elem_size(const gc_array *a)
{
	GC_ASSERT(a != NULL);
	return a->capacitive_container.container.allocator.elem_size;
}

static size_t array_capacity(const gc_array *a)
{
	GC_ASSERT(a != NULL);
	return a->capacitive_container.capacity;
}

static double array_capacity_ratio(const gc_array *a)
{
	GC_ASSERT(a != NULL);
	return a->capacitive_container.capacity_ratio;
}

static size_t array_count(const gc_array *a)
{
	GC_ASSERT(a != NULL);
	return a->capacitive_container.container.count;
}

static void array_allocate(gc_array *a, size_t capacity)
{
	GC_ASSERT(a != NULL);
	a->raw = gc_allocate((gc_allocator *)a, array_elem_size(a), capacity, a->raw);
	a->capacitive_container.capacity = capacity;
}

static void *array_at_raw(const void *raw, const size_t elem_size, gc_distance_t i)
{
	GC_ASSERT(raw != NULL);
	GC_ASSERT(elem_size != 0U);
	return (void *) & (((mem_unit *)raw)
					   [elem_size * i]);
}

static void array_dadvance_raw(gc_iterator *i, gc_distance_t d)
{
	size_t valsz = gc_value_size(i);
	GC_ASSERT(i != NULL);
	GC_ASSERT(valsz != 0U);
	i->it = array_at_raw(i->it, valsz, d);
	i->value = i->it;
}

static gc_array_iterator array_init_iterator_common(const gc_array *a,
		const gc_advance_function advance,
		const gc_radvance_function radvance,
		const gc_dadvance_function dadvance)
{
	gc_array_iterator ai;
	GC_ASSERT(a != NULL);
	GC_ASSERT(advance != NULL);
	GC_ASSERT(radvance != NULL);
	GC_ASSERT(dadvance != NULL);
	ai.bidirectional_iterator.forward_iterator.iterator.it = NULL;
	ai.bidirectional_iterator.forward_iterator.iterator.value = NULL;
	ai.bidirectional_iterator.forward_iterator.iterator.value_size =
		array_elem_size(a);
	ai.bidirectional_iterator.forward_iterator.advance = advance;
	ai.bidirectional_iterator.radvance = radvance;
	ai.dadvance = dadvance;
	return ai;
}

static void array_advance(void *it)
{
	GC_ASSERT(it != NULL);
	array_dadvance_raw((gc_iterator *)it, 1);
}

static void array_dadvance(void *it,
						   const gc_distance_t d)
{
	GC_ASSERT(it != NULL);
	array_dadvance_raw((gc_iterator *)it, d);
}

static void array_rdadvance(void *it,
							const gc_distance_t d)
{
	GC_ASSERT(it != NULL);
	array_dadvance(it, d * -1);
}

static void array_radvance(void *it)
{
	GC_ASSERT(it != NULL);
	array_dadvance_raw((gc_iterator *)it, -1);
}

static gc_array_iterator array_init_iterator(const gc_array *a)
{
	gc_array_iterator ai = array_init_iterator_common(a,
						&array_advance,
						&array_radvance,
						&array_dadvance);
	return ai;
}

static gc_array_iterator array_init_riterator(const gc_array *a)
{
	gc_array_iterator ai = array_init_iterator_common(a,
						&array_radvance,
						&array_advance,
						&array_rdadvance);
	return ai;
}

static void *array_at_unchecked(const gc_array *a, const size_t i)
{
	return array_at_raw(a->raw, array_elem_size(a), i);
}

void *array_at(const gc_array *a, const size_t i)
{
	GC_ASSERT(a != NULL);
	GC_ASSERT(i < array_count(a));
	return array_at_unchecked(a, i);
}

void *array_back(const gc_array *a)
{
	GC_ASSERT(a != NULL);
	GC_ASSERT(0U < array_count(a));
	return array_at(a, array_count(a) - 1);
}

gc_array_iterator array_begin(const gc_array *a)
{
	gc_array_iterator ai = array_init_iterator(a);
	GC_ASSERT(a != NULL);

	if (0 < array_count(a))
	{
		ai.bidirectional_iterator.forward_iterator.iterator.it = a->raw;
		ai.bidirectional_iterator.forward_iterator.iterator.value = a->raw;
	}

	return ai;
}

void array_clear(gc_array *a)
{
	GC_ASSERT(a != NULL);
	gc_deallocate((gc_allocator *)a, a->raw);
	a->capacitive_container.container.count = 0U;
	a->capacitive_container.capacity = 0U;
	a->raw = NULL;
}

gc_array_iterator array_end(const gc_array *a)
{
	gc_array_iterator ai = array_init_iterator(a);
	GC_ASSERT(a != NULL);

	if (0 < array_count(a))
		ai.bidirectional_iterator.forward_iterator.iterator.it =
			array_at_unchecked(a, array_count(a));

	return ai;
}

static gc_distance_t array_distance_raw(const void *b,
									 const void *e, size_t elem_size)
{
	GC_ASSERT(b != NULL);
	GC_ASSERT(e != NULL);
	GC_ASSERT(0U < elem_size);
	return (((mem_unit *)e) - ((mem_unit *)b)) / (gc_distance_t)elem_size;
}

static void array_erase_raw(gc_array *a,
							void *b, const void *e, size_t elem_size, size_t elems_beyond_e)
{
	GC_ASSERT(a != NULL);
	GC_ASSERT(b != NULL);
	GC_ASSERT(e != NULL);
	GC_ASSERT(0U < elem_size);
	mem_move(b, e, elems_beyond_e * elem_size);
	a->capacitive_container.container.count -=
		array_distance_raw(b, e, elem_size);
}

void array_erase(gc_array *a, gc_array_iterator *b, const gc_array_iterator *e)
{
	GC_ASSERT(a != NULL);
	GC_ASSERT(b != NULL);
	GC_ASSERT(e != NULL);

	if (b->bidirectional_iterator.forward_iterator.iterator.it ==
		e->bidirectional_iterator.forward_iterator.iterator.it)
	{
		return;
	}
	else if (b->bidirectional_iterator.forward_iterator.iterator.it <
			 e->bidirectional_iterator.forward_iterator.iterator.it)
		array_erase_raw(a,
						b->bidirectional_iterator.forward_iterator.iterator.it,
						e->bidirectional_iterator.forward_iterator.iterator.it,
						array_elem_size(a),
						array_distance_raw(
							e->bidirectional_iterator.forward_iterator.iterator.it,
							array_at_raw(
								a->raw,
								array_elem_size(a),
								array_count(a)),
							array_elem_size(a)));
	else
		array_erase_raw(a,
						array_at_raw(
							e->bidirectional_iterator.forward_iterator.iterator.it,
							array_elem_size(a),
							1),
						array_at_raw(
							b->bidirectional_iterator.forward_iterator.iterator.it,
							array_elem_size(a),
							1),
						array_elem_size(a),
						array_distance_raw(
							b->bidirectional_iterator.forward_iterator.iterator.it,
							array_at_raw(
								a->raw,
								array_elem_size(a),
								array_count(a)),
							array_elem_size(a)));

	array_allocate(a, array_count(a));
}

void *array_front(const gc_array *a)
{
	GC_ASSERT(a != NULL);
	GC_ASSERT(0U < array_count(a));
	return array_at(a, 0);
}

static size_t array_get_increased_capacity(gc_array *a, size_t cpcty)
{
	double cratio = array_capacity_ratio(a);
	size_t increasedCapacity = (size_t)(((double)cpcty) * cratio);

	if (increasedCapacity == cpcty)
	{
		return ++increasedCapacity;
	}

	return increasedCapacity;
}

static void array_guarantee_capacity(gc_array *a)
{
	if (array_count(a) == array_capacity(a))
		array_allocate(a,
					   array_get_increased_capacity(a, array_capacity(a)));
}

static void array_insert_raw(gc_array *a,
							 void *dst, const void *src, const size_t elem_size)
{
	mem_copy(dst, src, elem_size);
	++a->capacitive_container.container.count;
}

static void array_insert_before_end(gc_array *a,
									const void *v, const size_t i)
{
	GC_ASSERT(a != NULL);
	GC_ASSERT(v != NULL);
	GC_ASSERT(i < (array_count(a)) || (i == 0U && i == array_count(a)));
	array_guarantee_capacity(a);

	if (0 < array_count(a))
		mem_move(array_at_raw(a->raw, array_elem_size(a), i + 1),
				 array_at_raw(a->raw, array_elem_size(a), i),
				 array_distance_raw(array_at_raw(a->raw, array_elem_size(a), i),
									array_at_raw(a->raw, array_elem_size(a), array_count(a)),
									array_elem_size(a)) *
				 array_elem_size(a));

	array_insert_raw(a, array_at_raw(a->raw, array_elem_size(a), i), v, array_elem_size(a));
}

static void array_insert_at_end(gc_array *a, const void *v)
{
	GC_ASSERT(a != NULL);
	GC_ASSERT(v != NULL);
	array_guarantee_capacity(a);
	array_insert_raw(a,
					 array_at_raw(a->raw, array_elem_size(a), array_count(a)),
					 v,
					 array_elem_size(a));
}

void array_insert_after(gc_array *a, const gc_array_iterator *it, const void *v)
{
	gc_distance_t i = 0;
	GC_ASSERT(a != NULL);
	GC_ASSERT(it != NULL);
	GC_ASSERT(v != NULL);

	if (0 < array_count(a))
	{
		i = array_distance_raw(a->raw,
							   it->bidirectional_iterator.forward_iterator.iterator.it,
							   array_elem_size(a));
	}

	if (i < 0)
	{
		i = 0;
	}
	else
	{
		++i;
	}

	if (i < (gc_distance_t)array_count(a))
	{
		array_insert_before_end(a, v, i);
	}
	else
	{
		array_insert_at_end(a, v);
	}
}

void array_insert_before(gc_array *a, const gc_array_iterator *it, const void *v)
{
	gc_distance_t i = 0;
	GC_ASSERT(a != NULL);
	GC_ASSERT(it != NULL);
	GC_ASSERT(v != NULL);

	if (0U < array_count(a))
		i = array_distance_raw(a->raw,
							   it->bidirectional_iterator.forward_iterator.iterator.it,
							   array_elem_size(a));

	if (i < 0)
	{
		i = 0;
	}

	if (i < (gc_distance_t)array_count(a))
	{
		array_insert_before_end(a, v, i);
	}
	else
	{
		array_insert_at_end(a, v);
	}
}

void array_pop_back(gc_array *a)
{
	GC_ASSERT(a != NULL);
	GC_ASSERT(0U < array_count(a));
	--a->capacitive_container.container.count;
}

void array_pop_front(gc_array *a)
{
	GC_ASSERT(a != NULL);
	GC_ASSERT(0U < array_count(a));
	array_erase_raw(a,
					a->raw,
					array_at_raw(a->raw, array_elem_size(a), 1),
					array_elem_size(a),
					array_count(a) - 1);
}

void array_push_back(gc_array *a, const void *v)
{
	GC_ASSERT(a != NULL);
	GC_ASSERT(v != NULL);
	array_insert_at_end(a, v);
}

void array_push_front(gc_array *a, const void *v)
{
	GC_ASSERT(a != NULL);
	GC_ASSERT(v != NULL);
	array_insert_before_end(a, v, 0);
}

gc_array_iterator array_rbegin(const gc_array *a)
{
	gc_array_iterator ai = array_init_riterator(a);
	GC_ASSERT(a != NULL);

	if (0 < array_count(a))
	{
		ai.bidirectional_iterator.forward_iterator.iterator.it =
			array_at_unchecked(a, array_count(a) - 1);
		ai.bidirectional_iterator.forward_iterator.iterator.value =
			ai.bidirectional_iterator.forward_iterator.iterator.it;
	}

	return ai;
}

gc_array_iterator array_rend(const gc_array *a)
{
	gc_array_iterator ai = array_init_riterator(a);
	GC_ASSERT(a != NULL);

	if (0 < array_count(a))
		ai.bidirectional_iterator.forward_iterator.iterator.it =
			array_at_raw(a->raw, array_elem_size(a), -1);

	return ai;
}

void array_reserve(gc_array *a, const size_t r)
{
	size_t cnt = array_count(a);
	GC_ASSERT(a != NULL);

	if (r > cnt)
	{
		array_allocate(a, r);
	}
}

void array_resize(gc_array *a, const size_t s, const void *dv)
{
	size_t c = array_count(a);
	GC_ASSERT(a != NULL);
	GC_ASSERT(dv != NULL);
	array_allocate(a, s);

	for (; c < s; ++c)
	{
		mem_copy(array_at_raw(a->raw, array_elem_size(a), c), dv, array_elem_size(a));
	}

	a->capacitive_container.container.count = s;
}

void array_init_allocator(gc_array *a, gc_allocator al, double capacity_ratio)
{
	GC_ASSERT(a != NULL);
	GC_ASSERT(array_elem_size(a) != NULL);
	GC_ASSERT(1 < capacity_ratio);
	a->capacitive_container.container.allocator = al;
	a->capacitive_container.container.count = 0U;
	a->capacitive_container.capacity = 0U;
	a->capacitive_container.capacity_ratio = capacity_ratio;
	a->raw = NULL;
}

void array_init(gc_array *a, const size_t elem_size)
{
	GC_ASSERT(a != NULL);
	GC_ASSERT(elem_size != NULL);
	array_init_allocator(a, gc_default_allocator(elem_size), 1.5);
}

void array_uinit(gc_array *a)
{
	GC_ASSERT(a != NULL);
	gc_deallocate((gc_allocator *)a, a->raw);
	a->raw = NULL;
	a->capacitive_container.container.count = 0U;
	a->capacitive_container.capacity = 0U;
}
