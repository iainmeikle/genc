/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @slist algorithm.c
 */

#include "assert.h"
#include "memory.h"
#include <genc/allocator.h>
#include <genc/iterator.h>
#include <genc/slist.h>

static void slist_advance_raw(gc_iterator *it)
{
	gc_slist_node *sn = NULL;
	GC_ASSERT(it != NULL);
	sn = (gc_slist_node *)it->it;

	if (sn->next)
	{
		it->it = (void *)sn->next;
		it->value = (void *)sn->next->value;
	}
	else
	{
		it->it = NULL;
		it->value = NULL;
	}
}

static size_t slist_count(const gc_slist *sl)
{
	GC_ASSERT(sl != NULL);
	return sl->container.count;
}

static size_t slist_elem_size(const gc_slist *sl)
{
	GC_ASSERT(sl != NULL);
	return sl->container.allocator.elem_size;
}

static gc_slist_node *slist_create_slist_node(
	gc_slist *sl,
	const void *v)
{
	gc_slist_node *sn = (gc_slist_node *)gc_allocate(
						 (gc_allocator *)sl, sizeof(gc_slist_node), 1, 0);
	GC_ASSERT(sl != NULL);
	GC_ASSERT(v != NULL);
	GC_ASSERT(sn != NULL);
	sn->next = NULL;
	sn->value = gc_allocate((gc_allocator *)sl, slist_elem_size(sl), 1, 0);
	GC_ASSERT(sn->value != NULL);
	mem_copy(sn->value, v, slist_elem_size(sl));
	return sn;
}

static void slist_advance(void *it)
{
	GC_ASSERT(it != NULL);
	slist_advance_raw((gc_iterator *)it);
}

static gc_slist_iterator slist_init_iterator(const gc_slist *sl)
{
	gc_slist_iterator si;
	GC_ASSERT(sl != NULL);
	si.iterator.it = NULL;
	si.iterator.value = NULL;
	si.iterator.value_size = slist_elem_size(sl);
	si.advance = &slist_advance;
	return si;
}

static gc_slist_iterator slist_begin(const gc_slist *sl)
{
	GC_ASSERT(sl != NULL);

	if (slist_count(sl))
	{
		gc_slist_iterator si = slist_init_iterator(sl);
		si.iterator.it = sl->raw;
		si.iterator.value = sl->raw->value;
		return si;
	}

	return slist_init_iterator(sl);
}

static void slist_pop_front(gc_slist *sl);

static void slist_clear(gc_slist *sl)
{
	GC_ASSERT(sl != NULL);

	while (slist_count(sl))
	{
		slist_pop_front(sl);
	}
}

static gc_slist_iterator slist_end(const gc_slist *sl)
{
	GC_ASSERT(sl != NULL);
	return slist_init_iterator(sl);
}

static void *slist_front(const gc_slist *sl)
{
	GC_ASSERT(sl != NULL);
	GC_ASSERT(0U < slist_count(sl));
	GC_ASSERT(sl->raw != NULL);
	return sl->raw->value;
}

static void slist_pop_front(gc_slist *sl)
{
	gc_slist_node *sn = NULL;
	GC_ASSERT(sl != NULL);
	GC_ASSERT(0U < slist_count(sl));
	GC_ASSERT(sl->raw != NULL);
	sn = sl->raw;
	sl->raw = sl->raw->next;
	--sl->container.count;
	gc_deallocate((gc_allocator *)sl, sn->value);
	gc_deallocate((gc_allocator *)sl, sn);
}

static void slist_push_front(gc_slist *sl, const void *v)
{
	gc_slist_node *ln = slist_create_slist_node(sl, v);
	GC_ASSERT(sl != NULL);
	GC_ASSERT(v != NULL);
	GC_ASSERT(ln != NULL);
	ln->next = sl->raw;
	++sl->container.count;
	sl->raw = ln;
}

static void slist_resize(gc_slist *sl, const size_t s, const void *dv)
{
	GC_ASSERT(sl != NULL);
	GC_ASSERT(dv != NULL);

	while (slist_count(sl) < s)
	{
		slist_push_front(sl, dv);
	}

	while (s < slist_count(sl))
	{
		slist_pop_front(sl);
	}
}

void gc_slist_init_allocator(gc_slist *sl, gc_allocator al)
{
	GC_ASSERT(sl != NULL);
	sl->container.allocator = al;
	sl->container.count = 0U;
	sl->raw = NULL;
	sl->begin = &slist_begin;
	sl->clear = &slist_clear;
	sl->end = &slist_end;
	sl->front = &slist_front;
	sl->pop_front = &slist_pop_front;
	sl->push_front = &slist_push_front;
	sl->resize = &slist_resize;
}

void gc_slist_init(gc_slist *sl, const size_t elem_size)
{
	GC_ASSERT(sl != NULL);
	GC_ASSERT(elem_size != NULL);
	gc_slist_init_allocator(sl, gc_default_allocator(elem_size));
}

void gc_slist_uinit(gc_slist *sl)
{
	GC_ASSERT(sl != NULL);
	slist_clear(sl);
}
