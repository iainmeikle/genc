/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file list.h
 */

#ifndef D6C4B5F5_59EA_4A46_B4BF_592D71F9E4C3
#define D6C4B5F5_59EA_4A46_B4BF_592D71F9E4C3

#include <stddef.h>
#include "allocator.h"
#include "iterator.h"
#include "container.h"
#include "container_function_def.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * @brief Type definition for list iterators.
 */
typedef gc_bidirectional_iterator gc_list_iterator;

struct gc_list_TAG;

/**
 * @brief Type definition for list back function pointer.
 */
GC_DEF_CTNR_BACK_FUNC_PTR(gc_list);

/**
 * @brief Type definition for list begin function pointer.
 */
GC_DEF_CTNR_BEGIN_FUNC_PTR(gc_list);

/**
 * @brief Type definition for list clear function pointer.
 */
GC_DEF_CTNR_CLEAR_FUNC_PTR(gc_list);

/**
 * @brief Type definition for list end function pointer.
 */
GC_DEF_CTNR_END_FUNC_PTR(gc_list);

/**
 * @brief Type definition for list erase function pointer.
 */
GC_DEF_CTNR_ERASE_FUNC_PTR(gc_list);

/**
 * @brief Type definition for list front function pointer.
 */
GC_DEF_CTNR_FRONT_FUNC_PTR(gc_list);

/**
 * @brief Type definition for list insert_after function pointer.
 */
GC_DEF_CTNR_INSERT_AFTER_FUNC_PTR(gc_list);

/**
 * @brief Type definition for list insert_before function pointer.
 */
GC_DEF_CTNR_INSERT_BEFORE_FUNC_PTR(gc_list);

/**
 * @brief Type definition for list pop_back function pointer.
 */
GC_DEF_CTNR_POP_BACK_FUNC_PTR(gc_list);

/**
 * @brief Type definition for list pop_front function pointer.
 */
GC_DEF_CTNR_POP_FRONT_FUNC_PTR(gc_list);

/**
 * @brief Type definition for list push_back function pointer.
 */
GC_DEF_CTNR_PUSH_BACK_FUNC_PTR(gc_list);

/**
 * @brief Type definition for list push_front function pointer.
 */
GC_DEF_CTNR_PUSH_FRONT_FUNC_PTR(gc_list);

/**
 * @brief Type definition for list rbegin function pointer.
 */
GC_DEF_CTNR_RBEGIN_FUNC_PTR(gc_list);

/**
 * @brief Type definition for list rend function pointer.
 */
GC_DEF_CTNR_REND_FUNC_PTR(gc_list);

/**
 * @brief Type definition for list resize function pointer.
 */
GC_DEF_CTNR_RESIZE_FUNC_PTR(gc_list);

/**
 * @brief Structure to store information for a node within a list.
 */
typedef struct gc_list_node_TAG
{
	/**
	 * @brief Pointer to the value stored by the list node.
	 */
	void *value;

	/**
	 * @brief Pointer to the next node in the slist.
	 */
	struct gc_list_node_TAG *next;

	/**
	 * @brief Pointer to the previous node in the slist.
	 */
	struct gc_list_node_TAG *previous;
} gc_list_node;

/**
 * @brief Structure to store information required by an slist.
 */
typedef struct gc_list_TAG
{
	/**
	 * @brief The (internal) common container structure.
	 */
	gc_container container;

	/**
	 * @brief Function pointer for the back function.
	 */
	GC_DECL_CTNR_FUNC(gc_list, back);

	/**
	 * @brief Function pointer for the begin function.
	 */
	GC_DECL_CTNR_FUNC(gc_list, begin);

	/**
	 * @brief Function pointer for the clear function.
	 */
	GC_DECL_CTNR_FUNC(gc_list, clear);

	/**
	 * @brief Function pointer for the end function.
	 */
	GC_DECL_CTNR_FUNC(gc_list, end);

	/**
	 * @brief Function pointer for the erase function.
	 */
	GC_DECL_CTNR_FUNC(gc_list, erase);

	/**
	 * @brief Function pointer for the front function.
	 */
	GC_DECL_CTNR_FUNC(gc_list, front);

	/**
	 * @brief Function pointer for the insert_after function.
	 */
	GC_DECL_CTNR_FUNC(gc_list, insert_after);

	/**
	 * @brief Function pointer for the insert_before function.
	 */
	GC_DECL_CTNR_FUNC(gc_list, insert_before);

	/**
	 * @brief Function pointer for the pop_back function.
	 */
	GC_DECL_CTNR_FUNC(gc_list, pop_back);

	/**
	 * @brief Function pointer for the pop_front function.
	 */
	GC_DECL_CTNR_FUNC(gc_list, pop_front);

	/**
	 * @brief Function pointer for the push_back function.
	 */
	GC_DECL_CTNR_FUNC(gc_list, push_back);

	/**
	 * @brief Function pointer for the push_front function.
	 */
	GC_DECL_CTNR_FUNC(gc_list, push_front);

	/**
	 * @brief Function pointer for the rbegin function.
	 */
	GC_DECL_CTNR_FUNC(gc_list, rbegin);

	/**
	 * @brief Function pointer for the rend function.
	 */
	GC_DECL_CTNR_FUNC(gc_list, rend);

	/**
	 * @brief Function pointer for the resize function.
	 */
	GC_DECL_CTNR_FUNC(gc_list, resize);

	/**
	 * @brief Pointer to the first node in the list.
	 */
	gc_list_node *braw;

	/**
	 * @brief Pointer to the last node in the list.
	 */
	gc_list_node *eraw;
} gc_list;

/**
 * @brief Initialise the given list with the given allocator.
 * @param slist Pointer to the list to be initialised.
 * @param allocator The allocator for the list to use.
 */
void gc_list_init_allocator(gc_list * /*list*/, gc_allocator /*allocator*/);

/**
 * @brief Initialise the given list, with the given element size, using a
 * default allocator.
 * @param list Pointer to the list to be initialised.
 * @param element_size The size of each element, for use by a default allocator.
 */
void gc_list_init(gc_list * /*list*/, const size_t /*element_size*/);

/**
 * @brief Uninitialise the given list.
 * @param list Pointer to the list to be uninitialised.
 */
void gc_list_uinit(gc_list * /*list*/);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#include "container_function_udef.h"

#endif
