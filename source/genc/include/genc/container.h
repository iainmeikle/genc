/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file container.h
 */

#ifndef B166CC93_F604_40A6_A328_401B65D6C083
#define B166CC93_F604_40A6_A328_401B65D6C083

#include <stddef.h>
#include "boolean.h"
#include "allocator.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * @brief Structure common to all containers. This must be the first member in
 * all containers in order to allow correct casting.
 */
typedef struct gc_container_TAG
{
	/**
	 * @brief The allocator for the container. This must be the first member
	 * in order to allow for proper casting.
	 */
	gc_allocator allocator;

	/**
	 * @brief The number of elements within the container.
	 */
	size_t count;
} gc_container;

/**
 * @brief Structure common to all capacitive containers; containers which are
 * able to allocate resources up front. This must be the first member in a
 * capacitive container in order to allow correct casting.
 */
typedef struct gc_capacitive_container_TAG
{
	/**
	 * @brief The common container structure. This must be the first member in
	 * order to allow for correct casting.
	 */
	gc_container container;

	/**
	 * @brief The capacity of the container; the number of elements for which
	 * resources are allocated.
	 */
	size_t capacity;

	/**
	 * @brief The ratio by which the capacity is increased when not enough
	 * resources are allocated in order to store the number of required
	 * elements.
	 */
	double capacity_ratio;
} gc_capacitive_container;

/**
 * @brief Function to return the capacity of the given capacitive container.
 * @return The capacity of the given capacitive container.
 * @param capacitive_container Pointer to the capacitive container to query its
 * capacity.
 */
size_t gc_capacity(const void * /*capacitive_container*/);

/**
 * @brief Function to return the number of elements in the given container.
 * @return The number of elements in the given container.
 * @param container pointer to the container to query.
 */
size_t gc_count(const void * /*container*/);

/**
 * @brief Function to return whether or not the given container is empty i.e.
 * has a count of zero.
 * @return True if the given container is empty, false otherwise.
 * @param container Pointer to the container to query.
 */
gc_boolean gc_empty(const void * /*container*/);

/**
 * @brief Query the given capacitive container for ratio by which the capacity
 * should be increased when the container is full.
 * @return The ratio by which the capacity should be increased when the given
 * container is full and more memory is required.
 * @param capacitive_container Pointer to the capacitive container to be
 * queried.
 */
double gc_capacity_ratio(const void * /*capacitive_container*/);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif
