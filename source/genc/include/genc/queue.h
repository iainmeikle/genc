/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file queue.h
 */

#ifndef F807C766_2C31_4F4B_B340_D8D90F8AA9DA
#define F807C766_2C31_4F4B_B340_D8D90F8AA9DA

#include <stddef.h>
#include "allocator.h"
#include "iterator.h"
#include "container_function_def.h"
#include "array.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * @brief Type definition for queue iterators.
 */
typedef gc_array_iterator gc_queue_iterator;

struct gc_queue_TAG;

/**
 * @brief Type definition for queue back function pointer.
 */
GC_DEF_CTNR_BACK_FUNC_PTR(gc_queue);

/**
 * @brief Type definition for queue begin function pointer.
 */
GC_DEF_CTNR_BEGIN_FUNC_PTR(gc_queue);

/**
 * @brief Type definition for queue end function pointer.
 */
GC_DEF_CTNR_END_FUNC_PTR(gc_queue);

/**
 * @brief Type definition for queue front function pointer.
 */
GC_DEF_CTNR_FRONT_FUNC_PTR(gc_queue);

/**
 * @brief Type definition for queue pop_front function pointer.
 */
GC_DEF_CTNR_POP_FRONT_FUNC_PTR(gc_queue);

/**
 * @brief Type definition for queue push_back function pointer.
 */
GC_DEF_CTNR_PUSH_BACK_FUNC_PTR(gc_queue);

/**
 * @brief Type definition for queue reserve function pointer.
 */
GC_DEF_CTNR_RESERVE_FUNC_PTR(gc_queue);

/**
 * @brief Type definition for queue resize function pointer.
 */
GC_DEF_CTNR_RESIZE_FUNC_PTR(gc_queue);

/**
 * @brief Structure to store information required by a queue. A container
 * which uses a contigious block of memory to store elements.
 */
typedef struct gc_queue_TAG
{
	/**
	 * @brief The (internal) common array structure for containers which use a
	 * contigious memory layout.
	 */
	gc_array array;

	/**
	 * @brief Function pointer for the back function.
	 */
	GC_DECL_CTNR_FUNC(gc_queue, back);

	/**
	 * @brief Function pointer for the begin function.
	 */
	GC_DECL_CTNR_FUNC(gc_queue, begin);

	/**
	 * @brief Function pointer for the end function.
	 */
	GC_DECL_CTNR_FUNC(gc_queue, end);

	/**
	 * @brief Function pointer for the front function.
	 */
	GC_DECL_CTNR_FUNC(gc_queue, front);

	/**
	 * @brief Function pointer for the pop_front function.
	 */
	GC_DECL_CTNR_FUNC(gc_queue, pop_front);

	/**
	 * @brief Function pointer for the push_back function.
	 */
	GC_DECL_CTNR_FUNC(gc_queue, push_back);

	/**
	 * @brief Function pointer for the reserve function.
	 */
	GC_DECL_CTNR_FUNC(gc_queue, reserve);

	/**
	 * @brief Function pointer for the resize function.
	 */
	GC_DECL_CTNR_FUNC(gc_queue, resize);
} gc_queue;

/**
 * @brief Initialise the given queue, with the given allocator and capacity ratio.
 * @param queue Pointer to the queue to be initialised.
 * @param allocator The allocator for the queue to use.
 * @param capacity_ratio The ratio to increase the capacity of the queue by
 * when it becomes full and more space is required.
 */
void gc_queue_init_allocator(gc_queue * /*queue*/,
	gc_allocator /*allocator*/,
	double /*capacity_ratio*/);

/**
 * @brief Initialise the given queue, with the given element size, using a
 * default allocator.
 * @param queue Pointer to the queue to be initialised.
 * @param element_size The size of each element, for use by a default allocator.
 */
void gc_queue_init(gc_queue * /*queue*/, const size_t /*element_size*/);

/**
 * @brief Uninitialise the given queue.
 * @param queue Pointer to the queue to be uninitialised.
 */
void gc_queue_uinit(gc_queue * /*queue*/);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#include "container_function_udef.h"

#endif
