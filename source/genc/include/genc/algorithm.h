/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file allocator.h
 */

#ifndef DD03372E_EF31_4DE5_A415_22DD9CC3662A
#define DD03372E_EF31_4DE5_A415_22DD9CC3662A

#include <stddef.h>
#include "boolean.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

struct gc_iterator_TAG;
struct gc_forward_iterator_TAG;

/**
 * @brief The function pointer type of an accumulation function.
 * @param iterator The pointer to the iterator of the item to be accumulated.
 * @param accumulator Pointer to the type to store the accumulated value.
 */
typedef void (*gc_accumulate_function)(struct gc_iterator_TAG * /*iterator*/,
	void * /*accumulator*/);

/**
 * @brief Accumulates all items in the given range.
 * @param begin Pointer to the iterator representing the start of the range to
 * be accumulated.
 * @param end Pointer to the iterator representing the end of the range to be
 * accumulated.
 * @param accumulator Pointer to the type to store the accumulated value.
 * @param accumulate_function Pointer to the function to perform the
 * accumulation and store the result.
 */
void gc_accumulate(struct gc_forward_iterator_TAG * /*begin*/,
	const struct gc_forward_iterator_TAG * /*end*/,
    void * /*accumulator*/,
	const gc_accumulate_function /*accumulate_function*/);

/**
 * @brief Copies all items in the given range to the destination.
 * @param begin Pointer to the iterator representing the start of the range to
 * be copied.
 * @param end Pointer to the iterator representing the end of the range to be
 * copied.
 * @param destination Pointer to the iterator in the destination collection at
 * which copies will start being placed.
 */
void gc_copy(struct gc_forward_iterator_TAG * /*begin*/,
	const struct gc_forward_iterator_TAG * /*end*/,
	struct gc_forward_iterator_TAG * /*destination*/);

/**
 * @brief The function pointer type of a find comparison function.
 * @return True if the given iterator item matches, false otherwise.
 * @param iterator Pointer to the iterator of the current item to be compared.
 */
typedef gc_boolean(*gc_find_function)(struct gc_iterator_TAG * /*iterator*/);

/**
 * @brief Find the first item in the given range for which the comparison
 * function is satisfied.
 * @return True if the comparison function matches an item in the range, false
 * otherwise.
 * @param begin Pointer to the iterator representing the start of the range to
 * be searched.
 * @param end Pointer to the iterator representing the end of the range to
 * be searched.
 * @param find_function Function pointer to the comparison function.
 */
gc_boolean gc_find(struct gc_forward_iterator_TAG * /*begin*/,
	const struct gc_forward_iterator_TAG * /*end*/,
	const gc_find_function /*find_function*/);

/**
 * @brief The function pointer type of a function to act on a given element.
 * @param iterator Pointer to the iterator of the current item to be acted on.
 */
typedef void (*gc_for_each_function)(struct gc_iterator_TAG * /*iterator*/);

/**
 * @brief Iterate over the given range, calling the given function on each
 * item in that range.
 * @param begin Pointer to the iterator representing the start of the range to
 * be operated on.
 * @param end Pointer to the iterator representing the end of the range to be
 * operated on.
 * @param for_each_function Function pointer to the function to act on each item.
 */
void gc_for_each(struct gc_forward_iterator_TAG * /*begin*/,
	const struct gc_forward_iterator_TAG * /*end*/,
	const gc_for_each_function /*for_each_function*/);

/**
 * @brief Function to swap the given items.
 * @param iterator1 Pointer to the iterator of the first item to be swapped.
 * @param iterator2 Pointer to the iterator of the second item to be swapped.
 */
void gc_swap(struct gc_iterator_TAG * /*iterator1*/,
	struct gc_iterator_TAG * /*iterator2*/);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif
