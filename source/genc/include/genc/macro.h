/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file macro.h
 */

#ifndef A78F2E15_2E1A_49B1_A0EC_255AAB0CC83D
#define A78F2E15_2E1A_49B1_A0EC_255AAB0CC83D

/**
 * @brief A macro to invoke the at function pointer of the given container.
 * @param ctnr The container on which the function pointer is to be invoked.
 * @param i The index of the element.
 */
#define GC_AT(ctnr, i) (ctnr)->at((ctnr), (i))

/**
 * @brief A macro to invoke the back function pointer of the given container.
 * @param ctnr The container on which the function pointer is to be invoked.
 */
#define GC_BACK(ctnr) (ctnr)->back((ctnr))

/**
 * @brief A macro to invoke the begin function pointer of the given container.
 * @param ctnr The container on which the function pointer is to be invoked.
 */
#define GC_BEGIN(ctnr) (ctnr)->begin((ctnr))

/**
 * @brief A macro to invoke the clear function pointer of the given container.
 * @param ctnr The container on which the function pointer is to be invoked.
 */
#define GC_CLEAR(ctnr) (ctnr)->clear((ctnr))

/**
 * @brief A macro to invoke the end function pointer of the given container.
 * @param ctnr The container on which the function pointer is to be invoked.
 */
#define GC_END(ctnr) (ctnr)->end((ctnr))

/**
 * @brief A macro to invoke the erase function pointer of the given container.
 * @param ctnr The container on which the function pointer is to be invoked.
 * @param b The iterator from which elements should be erased (inclusive).
 * @param e The iterator to which elements should be erased (exclusive).
 */
#define GC_ERASE(ctnr, b, e) (ctnr)->erase((ctnr), (b), (e))

/**
 * @brief A macro to invoke the front function pointer of the given container.
 * @param ctnr The container on which the function pointer is to be invoked.
 */
#define GC_FRONT(ctnr) (ctnr)->front((ctnr))

/**
 * @brief A macro to invoke the insert_after function pointer of the given container.
 * @param ctnr The container on which the function pointer is to be invoked.
 * @param i The iterator after which the value is to be inserted.
 * @param v The value to be inserted.
 */
#define GC_INSERT_AFTER(ctnr, i, v) (ctnr)->insert_after((ctnr), (i), (v))

/**
 * @brief A macro to invoke the insert_before function pointer of the given container.
 * @param ctnr The container on which the function pointer is to be invoked.
 * @param i The iterator before which the value is to be inserted.
 * @param v The value to be inserted.
 */
#define GC_INSERT_BEFORE(ctnr, i, v) (ctnr)->insert_before((ctnr), (i), (v))

/**
 * @brief A macro to invoke the pop_back function pointer of the given container.
 * @param ctnr The container on which the function pointer is to be invoked.
 */
#define GC_POP_BACK(ctnr) (ctnr)->pop_back((ctnr))

/**
 * @brief A macro to invoke the pop_front function pointer of the given container.
 * @param ctnr The container on which the function pointer is to be invoked.
 */
#define GC_POP_FRONT(ctnr) (ctnr)->pop_front((ctnr))

/**
 * @brief A macro to invoke the push_back function pointer of the given container.
 * @param ctnr The container on which the function pointer is to be invoked.
 * @param v The value to be pushed on to the back of the container.
 */
#define GC_PUSH_BACK(ctnr, v) (ctnr)->push_back((ctnr), (v))

/**
 * @brief A macro to invoke the push_front function pointer of the given container.
 * @param ctnr The container on which the function pointer is to be invoked.
 * @param v The value to be pushed on to the front of the container.
 */
#define GC_PUSH_FRONT(ctnr, v) (ctnr)->push_front((ctnr), (v))

/**
 * @brief A macro to invoke the rbegin function pointer of the given container.
 * @param ctnr The container on which the function pointer is to be invoked.
 */
#define GC_RBEGIN(ctnr) (ctnr)->rbegin((ctnr))

/**
 * @brief A macro to invoke the rend function pointer of the given container.
 * @param ctnr The container on which the function pointer is to be invoked.
 */
#define GC_REND(ctnr) (ctnr)->rend((ctnr))

/**
 * @brief A macro to invoke the reserve function pointer of the given container.
 * @param ctnr The container on which the function pointer is to be invoked.
 * @param c The number of elements for which space is to be reserved.
 */
#define GC_RESERVE(ctnr, c) (ctnr)->reserve((ctnr), (c))

/**
 * @brief A macro to invoke the resize function pointer of the given container.
 * @param ctnr The container on which the function pointer is to be invoked.
 * @param c The number of elements which the container is to contain.
 * @param dv The default value which is to be used as padding where necessary.
 */
#define GC_RESIZE(ctnr, c, dv) (ctnr)->reserve((ctnr), (c), (dv))

/**
 * @brief A macro to invoke the capacity function on the given container.
 * @param ctnr The container on which the function is to be invoked.
 */
#define GC_CAPACITY(ctnr) capacity((const capacitive_container*)(ctnr))

/**
 * @brief A macro to invoke the count function on the given container.
 * @param ctnr The container on which the function is to be invoked.
 */
#define GC_COUNT(ctnr) count((const container*)(ctnr))

/**
 * @brief A macro to invoke the elem_size function on the given container.
 * @param ctnr The container on which the function is to be invoked.
 */
#define GC_ELEM_SIZE(ctnr) elem_size((const allocator*)(ctnr))

/**
 * @brief A macro to invoke the empty function on the given container.
 * @param ctnr The container on which the function is to be invoked.
 */
#define GC_EMPTY(ctnr) empty((const container*)(ctnr))

/**
 * @brief A macro to invoke the advance function on the given iterator.
 * @param iter The iterator on which the function is to be invoked.
 */
#define GC_ADVANCE(iter) advance((iter))

/**
 * @brief A macro to invoke the radvance function on the given iterator.
 * @param iter The iterator on which the function is to be invoked.
 */
#define GC_RADVANCE(iter) radvance((iter))

/**
 * @brief A macro to invoke the dadvance function on the given iterator.
 * @param iter The iterator on which the function is to be invoked.
 * @param d The distance to advance the iterator.
 */
#define GC_DADVCANCE(iter, d) dadvance((iter), (d))

/**
 * @brief A macro to invoke the value_size function on the given iterator.
 * @param iter The iterator on which the function is to be invoked.
 */
#define GC_VALUE_SIZE(iter) value_size((iter))

#endif
