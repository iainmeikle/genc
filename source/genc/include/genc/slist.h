/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file slist.h
 */

#ifndef F3AEF37C_0CCF_4BBF_83D2_4DCC4CD56CA1
#define F3AEF37C_0CCF_4BBF_83D2_4DCC4CD56CA1

#include <stddef.h>
#include "allocator.h"
#include "iterator.h"
#include "container.h"
#include "container_function_def.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * @brief Type definition for slist iterators.
 */
typedef gc_forward_iterator gc_slist_iterator;

struct gc_slist_TAG;

/**
 * @brief Type definition for slist begin function pointer.
 */
GC_DEF_CTNR_BEGIN_FUNC_PTR(gc_slist);

/**
 * @brief Type definition for slist clear function pointer.
 */
GC_DEF_CTNR_CLEAR_FUNC_PTR(gc_slist);

/**
 * @brief Type definition for slist end function pointer.
 */
GC_DEF_CTNR_END_FUNC_PTR(gc_slist);

/**
 * @brief Type definition for slist front function pointer.
 */
GC_DEF_CTNR_FRONT_FUNC_PTR(gc_slist);

/**
 * @brief Type definition for slist pop_front function pointer.
 */
GC_DEF_CTNR_POP_FRONT_FUNC_PTR(gc_slist);

/**
 * @brief Type definition for slist push_front function pointer.
 */
GC_DEF_CTNR_PUSH_FRONT_FUNC_PTR(gc_slist);

/**
 * @brief Type definition for slist resize function pointer.
 */
GC_DEF_CTNR_RESIZE_FUNC_PTR(gc_slist);

/**
 * @brief Structure to store information for a node within an slist.
 */
typedef struct gc_slist_node_TAG
{
	/**
	 * @brief Pointer to the value stored by the slist node.
	 */
	void *value;

	/**
	 * @brief Pointer to the next node in the slist.
	 */
	struct gc_slist_node_TAG *next;
} gc_slist_node;

/**
 * @brief Structure to store information required by an slist.
 */
typedef struct gc_slist_TAG
{
	/**
	 * @brief The (internal) common container structure.
	 */
	gc_container container;

	/**
	 * @brief Function pointer for the begin function.
	 */
	GC_DECL_CTNR_FUNC(gc_slist, begin);

	/**
	 * @brief Function pointer for the clear function.
	 */
	GC_DECL_CTNR_FUNC(gc_slist, clear);

	/**
	 * @brief Function pointer for the end function.
	 */
	GC_DECL_CTNR_FUNC(gc_slist, end);

	/**
	 * @brief Function pointer for the front function.
	 */
	GC_DECL_CTNR_FUNC(gc_slist, front);

	/**
	 * @brief Function pointer for the pop_front function.
	 */
	GC_DECL_CTNR_FUNC(gc_slist, pop_front);

	/**
	 * @brief Function pointer for the push_front function.
	 */
	GC_DECL_CTNR_FUNC(gc_slist, push_front);

	/**
	 * @brief Function pointer for the resize function.
	 */
	GC_DECL_CTNR_FUNC(gc_slist, resize);

	/**
	 * @brief Pointer to the first node in the slist.
	 */
	gc_slist_node *raw;
} gc_slist;

/**
 * @brief Initialise the given slist with the given allocator.
 * @param slist Pointer to the slist to be initialised.
 * @param allocator The allocator for the slist to use.
 */
void gc_slist_init_allocator(gc_slist * /*slist*/, gc_allocator /*allocator*/);

/**
 * @brief Initialise the given slist, with the given element size, using a
 * default allocator.
 * @param slist Pointer to the slist to be initialised.
 * @param element_size The size of each element, for use by a default allocator.
 */
void gc_slist_init(gc_slist * /*slist*/, const size_t /*element_size*/);

/**
 * @brief Uninitialise the given slist.
 * @param slist Pointer to the slist to be uninitialised.
 */
void gc_slist_uinit(gc_slist * /*slist*/);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#include "container_function_udef.h"

#endif
