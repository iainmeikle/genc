/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file iterator.h
 */

#ifndef CA94DD4B_E3E3_4987_82A2_83B7862BFC0B
#define CA94DD4B_E3E3_4987_82A2_83B7862BFC0B

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * @brief Type to represent the distance between elements.
 */
typedef long gc_distance_t;

struct gc_forward_iterator_TAG;

/**
 * @brief Definition of advance function pointer signature.
 */
typedef void (*gc_advance_function)(void *);

struct gc_bidirectional_iterator_TAG;

/**
 * @brief Definition of radvance function pointer signature.
 */
typedef void (*gc_radvance_function)(void *);

struct gc_random_access_iterator_TAG;

/**
 * @brief Definition of dadvance function pointer signature.
 */
typedef void (*gc_dadvance_function)(void *,
	gc_distance_t);

/**
 * @brief Structure common to all types of iterator. This must be the first
 * member in all iterators in order to allow correct casting.
 */
typedef struct gc_iterator_TAG
{
	/**
	 * @brief Pointer to the value referenced by the iterator.
	 */
	void *value;

	/**
	 * @brief Pointer to the internal iterator referenced by the iterator.
	 */
	void *it;

	/**
	 * @brief The size of the value pointed to by the value member.
	 */
	size_t value_size;
} gc_iterator;

/**
 * @brief Structure common to all types of forward iterator. This must be the
 * first member in all forward iterators in order to allow correct casting.
 */
typedef struct gc_forward_iterator_TAG
{
	/**
	 * @brief The common iterator component of the forward iterator. This must
	 * be the first member in order to allow correct casting.
	 */
	gc_iterator iterator;

	/**
	 * @brief Function pointer to the advance function used to advance the
	 * iterator.
	 */
	gc_advance_function advance;
} gc_forward_iterator;

/**
 * @brief Structure common to all types of bidirectional iterator. This must
 * be the first member in all bidirectional iterators in order to allow
 * correct casting.
 */
typedef struct gc_bidirectional_iterator_TAG
{
	/**
	 * @brief The common forward iterator component of the bidirectional
	 * iterator. This must be the first member in order to allow correct
	 * casting.
	 */
	gc_forward_iterator forward_iterator;

	/**
	 * @brief Function pointer to the radvance function used to advance the iterator in
	 * the opposite direction.
	 */
	gc_radvance_function radvance;
} gc_bidirectional_iterator;

/**
 * @brief Structure common to all types of random access iterator. This must
 * be the first member in all random access iterators in order to allow
 * correct casting.
 */
typedef struct gc_random_access_iterator_TAG
{
	/**
	 * @brief The common bidirectional iterator component of the random access
	 * iterator. This must be the first member in order to allow correct
	 * casting.
	 */
	gc_bidirectional_iterator bidirectional_iterator;

	/**
	 * @brief Function pointer to the dadvance function used to advance the
	 * iterator by the specified distance.
	 */
	gc_dadvance_function dadvance;
} gc_random_access_iterator;

/**
 * @brief Advance the given iterator by one.
 * @param forward_iterator Pointer to the forward iterator to advance.
 */
void gc_advance(void * /*forward_iterator*/);

/**
 * @brief Advance the given iterator by negative one (reverse advance it).
 * @param bidirectional_iterator Pointer to the bidirectional iterator to
 * advance.
 */
void gc_radvance(void * /*bidirectional_iterator*/);

/**
 * @brief Advance the given iterator by the specified distance.
 * @param random_access_iterator Pointer to the random access iterator to be
 * advanced / radvanced.
 * @param distance The distance to advance (positive numbers) / radvance
 * (negative numbers) the given iterator.
 */
void gc_dadvance(void * /*random_access_iterator*/,
	const gc_distance_t /*distance*/);

/**
 * @brief Query the value size for the given iterator.
 * @return The size of the value held by the given iterator.
 * @param iterator Pointer to the iterator to be queried.
 */
size_t gc_value_size(const void * /*iterator*/);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif
