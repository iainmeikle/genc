/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file array.h
 */

#ifndef F357F264_C91B_43A4_AD18_DE1614E97AD9
#define F357F264_C91B_43A4_AD18_DE1614E97AD9

#include <stddef.h>
#include "allocator.h"
#include "iterator.h"
#include "container.h"

/**
 * @brief Type definition for (internal) common array iterators.
 */
typedef gc_random_access_iterator gc_array_iterator;

/**
 * @brief The (internal) common array structure for containers which use a
 * contigious memory layout.
 */
typedef struct gc_array_TAG
{
	/**
	 * @brief The common capacitive_container structure. This must be the first
	 * member in order to allow for correct casting.
	 */
	gc_capacitive_container capacitive_container;

	/**
	 * @brief Void pointer to the raw contigious memory block used to store
	 * elements held by the array.
	 */
	void *raw;
} gc_array;

#endif
