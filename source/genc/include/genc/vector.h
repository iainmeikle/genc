/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file vector.h
 */

#ifndef B3F52CCC_F123_4B0F_8A84_E5BD0BA8A0E1
#define B3F52CCC_F123_4B0F_8A84_E5BD0BA8A0E1

#include <stddef.h>
#include "iterator.h"
#include "container_function_def.h"
#include "array.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * @brief Type definition for vector iterators.
 */
typedef gc_array_iterator gc_vector_iterator;

struct gc_vector_TAG;

/**
 * @brief Type definition for vector at function pointer.
 */
GC_DEF_CTNR_AT_FUNC_PTR(gc_vector);

/**
 * @brief Type definition for vector back function pointer.
 */
GC_DEF_CTNR_BACK_FUNC_PTR(gc_vector);

/**
 * @brief Type definition for vector begin function pointer.
 */
GC_DEF_CTNR_BEGIN_FUNC_PTR(gc_vector);

/**
 * @brief Type definition for vector clear function pointer.
 */
GC_DEF_CTNR_CLEAR_FUNC_PTR(gc_vector);

/**
 * @brief Type definition for vector end function pointer.
 */
GC_DEF_CTNR_END_FUNC_PTR(gc_vector);

/**
 * @brief Type definition for vector erase function pointer.
 */
GC_DEF_CTNR_ERASE_FUNC_PTR(gc_vector);

/**
 * @brief Type definition for vector front function pointer.
 */
GC_DEF_CTNR_FRONT_FUNC_PTR(gc_vector);

/**
 * @brief Type definition for vector insert_after function pointer.
 */
GC_DEF_CTNR_INSERT_AFTER_FUNC_PTR(gc_vector);

/**
 * @brief Type definition for vector insert_before function pointer.
 */
GC_DEF_CTNR_INSERT_BEFORE_FUNC_PTR(gc_vector);

/**
 * @brief Type definition for vector pop_back function pointer.
 */
GC_DEF_CTNR_POP_BACK_FUNC_PTR(gc_vector);

/**
 * @brief Type definition for vector pop_front function pointer.
 */
GC_DEF_CTNR_POP_FRONT_FUNC_PTR(gc_vector);

/**
 * @brief Type definition for vector push_back function pointer.
 */
GC_DEF_CTNR_PUSH_BACK_FUNC_PTR(gc_vector);

/**
 * @brief Type definition for vector push_front function pointer.
 */
GC_DEF_CTNR_PUSH_FRONT_FUNC_PTR(gc_vector);

/**
 * @brief Type definition for vector rbegin function pointer.
 */
GC_DEF_CTNR_RBEGIN_FUNC_PTR(gc_vector);

/**
 * @brief Type definition for vector rend function pointer.
 */
GC_DEF_CTNR_REND_FUNC_PTR(gc_vector);

/**
 * @brief Type definition for vector reserve function pointer.
 */
GC_DEF_CTNR_RESERVE_FUNC_PTR(gc_vector);

/**
 * @brief Type definition for vector resize function pointer.
 */
GC_DEF_CTNR_RESIZE_FUNC_PTR(gc_vector);

/**
 * @brief Structure to store information required by a vector. A container
 * which uses a contigious block of memory to store elements.
 */
typedef struct gc_vector_TAG
{
	/**
	 * @brief The (internal) common array structure for containers which use a
	 * contigious memory layout.
	 */
	gc_array array;

	/**
	 * @brief Function pointer for the at function.
	 */
	GC_DECL_CTNR_FUNC(gc_vector, at);

	/**
	 * @brief Function pointer for the back function.
	 */
	GC_DECL_CTNR_FUNC(gc_vector, back);

	/**
	 * @brief Function pointer for the begin function.
	 */
	GC_DECL_CTNR_FUNC(gc_vector, begin);

	/**
	 * @brief Function pointer for the clear function.
	 */
	GC_DECL_CTNR_FUNC(gc_vector, clear);

	/**
	 * @brief Function pointer for the end function.
	 */
	GC_DECL_CTNR_FUNC(gc_vector, end);

	/**
	 * @brief Function pointer for the erase function.
	 */
	GC_DECL_CTNR_FUNC(gc_vector, erase);

	/**
	 * @brief Function pointer for the front function.
	 */
	GC_DECL_CTNR_FUNC(gc_vector, front);

	/**
	 * @brief Function pointer for the insert_after function.
	 */
	GC_DECL_CTNR_FUNC(gc_vector, insert_after);

	/**
	 * @brief Function pointer for the insert_before function.
	 */
	GC_DECL_CTNR_FUNC(gc_vector, insert_before);

	/**
	 * @brief Function pointer for the pop_back function.
	 */
	GC_DECL_CTNR_FUNC(gc_vector, pop_back);

	/**
	 * @brief Function pointer for the pop_front function.
	 */
	GC_DECL_CTNR_FUNC(gc_vector, pop_front);

	/**
	 * @brief Function pointer for the push_back function.
	 */
	GC_DECL_CTNR_FUNC(gc_vector, push_back);

	/**
	 * @brief Function pointer for the push_front function.
	 */
	GC_DECL_CTNR_FUNC(gc_vector, push_front);

	/**
	 * @brief Function pointer for the rbegin function.
	 */
	GC_DECL_CTNR_FUNC(gc_vector, rbegin);

	/**
	 * @brief Function pointer for the rend function.
	 */
	GC_DECL_CTNR_FUNC(gc_vector, rend);

	/**
	 * @brief Function pointer for the reserve function.
	 */
	GC_DECL_CTNR_FUNC(gc_vector, reserve);

	/**
	 * @brief Function pointer for the resize function.
	 */
	GC_DECL_CTNR_FUNC(gc_vector, resize);
} gc_vector;

/**
 * @brief Initialise the given vector with the given allocator.
 * @param vector Pointer to the vector to be initialised.
 * @param allocator The allocator for the vector to use.
 * @param capacity_ratio The ratio to increase the capacity of the vector by
 * when it becomes full and more space is required.
 */
void gc_vector_init_allocator(gc_vector * /*vector*/,
	gc_allocator /*allocator*/,
	double /*capacity_ratio*/);

/**
 * @brief Initialise the given vector, with the given element size, using a
 * default allocator.
 * @param vector Pointer to the vector to be initialised.
 * @param element_size The size of each element that the vector is to manage.
 */
void gc_vector_init(gc_vector * /*vector*/, const size_t /*element_size*/);

/**
 * @brief Uninitialise the given vector.
 * @param vector Pointer to the vector to be uninitialised.
 */
void gc_vector_uinit(gc_vector * /*vector*/);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#include "container_function_udef.h"

#endif
