/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file allocator.h
 */

#ifndef D3A7F5D8_96B3_456B_8E8E_976719C24899
#define D3A7F5D8_96B3_456B_8E8E_976719C24899

#include <stddef.h>
#include "assert.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * @brief Function pointer type for an allocate function.
 * @return The allocated memory on success, null otherwise.
 * @param elem_size The size of each element for which to allocate memory.
 * @param num_elems The number of elements for which to allocate memory.
 * @param mem_ptr Pointer to an already allocated block of memory to be
 * reallocated, null otherwise.
 */
typedef void *(*gc_allocate_function)(size_t /*elem_size*/,
	size_t /*num_elems*/, void * /*mem_ptr*/);

/**
 * @brief Function pointer type for a deallocate function.
 * @param mem_ptr Pointe to the block of memory to be deallocated.
 */
typedef void (*gc_deallocate_function)(void * /*mem_ptr*/);

/**
 * @brief A structure to represent an allocator.
 */
typedef struct gc_allocator_TAG
{
	/**
	 * @brief Pointer to the allocation function.
	 */
	gc_allocate_function allocate;

	/**
	 * @brief Pointer to the deallocation function.
	 */
	gc_deallocate_function deallocate;

	/**
	 * @brief The size of each element.
	 */
	size_t elem_size;
} gc_allocator;

/**
 * @brief Function to create a default allocator instance for a given element
 * size.
 * @return An allocator instance with the given element size.
 * @param elem_size The element size for the allocator to be created.
 */
gc_allocator gc_default_allocator(const size_t /*elem_size*/);

/**
 * @brief Allocate the specified number of element, with the specified size,
 * using the given allocator. If memory is already allocated, then reallocate
 * using the given pointer.
 * @return Pointer to the allocated or reallocated memory.
 * @param allocator Pointer to the allocator to use to allocate memory.
 * @param element_size The size of each element to be allocated.
 * @param number_of_elements The number of elements to be allocated.
 * @param memory Pointer to already allocated memory, null otherwise.
 */
void *gc_allocate(gc_allocator * /*allocator*/, size_t /*element_size*/,
	size_t /*number_of_elements*/, void * /*memory*/);

/**
 * @brief Deallocate the given chunk of memory using the given allocator.
 * @param allocator Pointer to the allocator to use to deallocate the given
 * memory.
 * @param memory Pointer to the memory to be deallocated.
 */
void gc_deallocate(gc_allocator * /*allocator*/, void * /*memory*/);

/**
 * @brief Function to retrieve the size of each element for the given
 * allocator.
 * @return The size of an element for the given allocator.
 * @param allocator Pointer to the allocator to interrogate for its element
 * size.
 */
size_t gc_elem_size(const void * /*allocator*/);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif
