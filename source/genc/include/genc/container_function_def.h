/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file container_function_def.h
 */

#define CD17A213_C922_45EF_8D81_92A4EB3C26FE

/**
 * @brief A macro to declare an at function pointer type definition with
 * uniform signature in order to preserve a standard calling convention for the
 * specified container.
 * @param ctnr The name of the container.
 */
#define GC_DEF_CTNR_AT_FUNC_PTR(ctnr) typedef void* (*ctnr##_at_function) \
	(const struct ctnr##_TAG*, const size_t)

/**
 * @brief A macro to declare a back function pointer type definition with
 * uniform signature in order to preserve a standard calling convention for the
 * specified container.
 * @param ctnr The name of the container.
 */
#define GC_DEF_CTNR_BACK_FUNC_PTR(ctnr) typedef void* (*ctnr##_back_function) \
	(const struct ctnr##_TAG*)

/**
 * @brief A macro to declare a begin function pointer type definition with
 * uniform signature in order to preserve a standard calling convention for the
 * specified container.
 * @param ctnr The name of the container.
 */
#define GC_DEF_CTNR_BEGIN_FUNC_PTR(ctnr) typedef ctnr##_iterator \
	(*ctnr##_begin_function)(const struct ctnr##_TAG*)

/**
 * @brief A macro to declare a clear function pointer type definition with
 * uniform signature in order to preserve a standard calling convention for the
 * specified container.
 * @param ctnr The name of the container.
 */
#define GC_DEF_CTNR_CLEAR_FUNC_PTR(ctnr) typedef void (*ctnr##_clear_function) \
	(struct ctnr##_TAG*)

/**
 * @brief A macro to declare an end function pointer type definition with
 * uniform signature in order to preserve a standard calling convention for the
 * specified container.
 * @param ctnr The name of the container.
 */
#define GC_DEF_CTNR_END_FUNC_PTR(ctnr) typedef ctnr##_iterator \
	(*ctnr##_end_function)(const struct ctnr##_TAG*)

/**
 * @brief A macro to declare an erase function pointer type definition with
 * uniform signature in order to preserve a standard calling convention for the
 * specified container.
 * @param ctnr The name of the container.
 */
#define GC_DEF_CTNR_ERASE_FUNC_PTR(ctnr) typedef void (*ctnr##_erase_function) \
	(struct ctnr##_TAG*, ctnr##_iterator*, const ctnr##_iterator*)

/**
 * @brief A macro to declare a front function pointer type definition with
 * uniform signature in order to preserve a standard calling convention for the
 * specified container.
 * @param ctnr The name of the container.
 */
#define GC_DEF_CTNR_FRONT_FUNC_PTR(ctnr) typedef void* (*ctnr##_front_function) \
	(const struct ctnr##_TAG*)

/**
 * @brief A macro to declare an insert_after function pointer type definition
 * with uniform signature in order to preserve a standard calling convention
 * for the specified container.
 * @param ctnr The name of the container.
 */
#define GC_DEF_CTNR_INSERT_AFTER_FUNC_PTR(ctnr) typedef void (*ctnr##_insert_after_function) \
	(struct ctnr##_TAG*, const ctnr##_iterator*, const void*)

/**
 * @brief A macro to declare an insert_before function pointer type definition
 * with uniform signature in order to preserve a standard calling convention
 * for the specified container.
 * @param ctnr The name of the container.
 */
#define GC_DEF_CTNR_INSERT_BEFORE_FUNC_PTR(ctnr) typedef void (*ctnr##_insert_before_function) \
	(struct ctnr##_TAG*, const ctnr##_iterator*, const void*)

/**
 * @brief A macro to declare a pop_back at function pointer type definition
 * with uniform signature in order to preserve a standard calling convention
 * for the specified container.
 * @param ctnr The name of the container.
 */
#define GC_DEF_CTNR_POP_BACK_FUNC_PTR(ctnr) typedef void \
	(*ctnr##_pop_back_function)(struct ctnr##_TAG*)

/**
 * @brief A macro to declare a pop_front at function pointer type definition
 * with uniform signature in order to preserve a standard calling convention
 * for the specified container.
 * @param ctnr The name of the container.
 */
#define GC_DEF_CTNR_POP_FRONT_FUNC_PTR(ctnr) typedef void \
	(*ctnr##_pop_front_function)(struct ctnr##_TAG*)

/**
 * @brief A macro to declare a push_back at function pointer type definition
 * with uniform signature in order to preserve a standard calling convention
 * for the specified container.
 * @param ctnr The name of the container.
 */
#define GC_DEF_CTNR_PUSH_BACK_FUNC_PTR(ctnr) typedef void \
	(*ctnr##_push_back_function)(struct ctnr##_TAG*, const void*)

/**
 * @brief A macro to declare a push_front at function pointer type definition
 * with uniform signature in order to preserve a standard calling convention
 * for the specified container.
 * @param ctnr The name of the container.
 */
#define GC_DEF_CTNR_PUSH_FRONT_FUNC_PTR(ctnr) typedef void \
	(*ctnr##_push_front_function)(struct ctnr##_TAG*, const void*)

/**
 * @brief A macro to declare an rbegin function pointer type definition with
 * uniform signature in order to preserve a standard calling convention for the
 * specified container.
 * @param ctnr The name of the container.
 */
#define GC_DEF_CTNR_RBEGIN_FUNC_PTR(ctnr) typedef ctnr##_iterator \
	(*ctnr##_rbegin_function)(const struct ctnr##_TAG*)

/**
 * @brief A macro to declare an rend function pointer type definition with
 * uniform signature in order to preserve a standard calling convention for the
 * specified container.
 * @param ctnr The name of the container.
 */
#define GC_DEF_CTNR_REND_FUNC_PTR(ctnr) typedef ctnr##_iterator \
	(*ctnr##_rend_function)(const struct ctnr##_TAG*)

/**
 * @brief A macro to declare a reserve function pointer type definition with
 * uniform signature in order to preserve a standard calling convention for the
 * specified container.
 * @param ctnr The name of the container.
 */
#define GC_DEF_CTNR_RESERVE_FUNC_PTR(ctnr) typedef void (*ctnr##_reserve_function) \
	(struct ctnr##_TAG*, const size_t)

/**
 * @brief A macro to declare a resize function pointer type definition with
 * uniform signature in order to preserve a standard calling convention for the
 * specified container.
 * @param ctnr The name of the container.
 */
#define GC_DEF_CTNR_RESIZE_FUNC_PTR(ctnr) typedef void (*ctnr##_resize_function) \
	(struct ctnr##_TAG*, const size_t, const void*)

/**
 * @brief Declare the specified function on the specified container.
 * @param ctnr The type of the container on which the function is to be
 * declared.
 * @param func The name of the function to be declared.
 */
#define GC_DECL_CTNR_FUNC(ctnr, func) ctnr##_##func##_function func
