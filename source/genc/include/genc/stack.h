/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file stack.h
 */

#ifndef C8CF4D8B_B108_4D76_9D0C_4C0D16B64E36
#define C8CF4D8B_B108_4D76_9D0C_4C0D16B64E36

#include <stddef.h>
#include "allocator.h"
#include "iterator.h"
#include "container_function_def.h"
#include "array.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * @brief Type definition for stack iterators.
 */
typedef gc_array_iterator gc_stack_iterator;

struct gc_stack_TAG;

/**
 * @brief Type definition for stack begin function pointer.
 */
GC_DEF_CTNR_BEGIN_FUNC_PTR(gc_stack);

/**
 * @brief Type definition for stack end function pointer.
 */
GC_DEF_CTNR_END_FUNC_PTR(gc_stack);

/**
 * @brief Type definition for stack front function pointer.
 */
GC_DEF_CTNR_FRONT_FUNC_PTR(gc_stack);

/**
 * @brief Type definition for stack pop_front function pointer.
 */
GC_DEF_CTNR_POP_FRONT_FUNC_PTR(gc_stack);

/**
 * @brief Type definition for stack push_front function pointer.
 */
GC_DEF_CTNR_PUSH_FRONT_FUNC_PTR(gc_stack);

/**
 * @brief Type definition for stack reserve function pointer.
 */
GC_DEF_CTNR_RESERVE_FUNC_PTR(gc_stack);

/**
 * @brief Type definition for stack resize function pointer.
 */
GC_DEF_CTNR_RESIZE_FUNC_PTR(gc_stack);

/**
 * @brief Structure to store information required by a stack. A container
 * which uses a contigious block of memory to store elements.
 */
typedef struct gc_stack_TAG
{
	/**
	 * @brief The (internal) common array structure for containers which use a
	 * contigious memory layout.
	 */
	gc_array array;

	/**
	 * @brief Function pointer for the begin function.
	 */
	GC_DECL_CTNR_FUNC(gc_stack, begin);

	/**
	 * @brief Function pointer for the end function.
	 */
	GC_DECL_CTNR_FUNC(gc_stack, end);

	/**
	 * @brief Function pointer for the front function.
	 */
	GC_DECL_CTNR_FUNC(gc_stack, front);

	/**
	 * @brief Function pointer for the pop_front function.
	 */
	GC_DECL_CTNR_FUNC(gc_stack, pop_front);

	/**
	 * @brief Function pointer for the push_front function.
	 */
	GC_DECL_CTNR_FUNC(gc_stack, push_front);

	/**
	 * @brief Function pointer for the reserve function.
	 */
	GC_DECL_CTNR_FUNC(gc_stack, reserve);

	/**
	 * @brief Function pointer for the resize function.
	 */
	GC_DECL_CTNR_FUNC(gc_stack, resize);
} gc_stack;

/**
 * @brief Initialise the given stack, with the given allocator and capacity ratio.
 * @param stack Pointer to the stack to be initialised.
 * @param allocator The allocator for the stack to use.
 * @param capacity_ratio The ratio to increase the capacity of the stack by
 * when it becomes full and more space is required.
 */
void gc_stack_init_allocator(gc_stack * /*stack*/,
	gc_allocator /*allocator*/,
	double /*capacity_ratio*/);

/**
 * @brief Initialise the given stack, with the given element size, using a
 * default allocator.
 * @param stack Pointer to the stack to be initialised.
 * @param element_size The size of each element, for use by a default allocator.
 */
void gc_stack_init(gc_stack * /*stack*/, const size_t /*element_size*/);

/**
 * @brief Uninitialise the given stack.
 * @param stack Pointer to the stack to be uninitialised.
 */
void gc_stack_uinit(gc_stack * /*stack*/);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#include "container_function_udef.h"

#endif
