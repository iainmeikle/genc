/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file boolean.h
 */

#ifndef D8CA7C82_722E_4864_A21D_E35698247C41
#define D8CA7C82_722E_4864_A21D_E35698247C41

/**
 * @brief Enumeration to represent a well defined boolean type and overcome the
 * limitations of C89.
 */
typedef enum
{
	/**
	 * @brief Value to represent false.
	 */
	_false = 0,

	/**
	 * @brief Value to represent true.
	 */
	_true = 1
} gc_boolean;

#endif
