/**
 * @author Iain Meikle
 * @copyright Iain Meikle
 * @file container_function_udef.h
 */

#ifndef CD17A213_C922_45EF_8D81_92A4EB3C26FE
#error "The header container_function_def.h must be included first."
#endif

#undef GC_DEF_CTNR_AT_FUNC_PTR

#undef GC_DEF_CTNR_BACK_FUNC_PTR

#undef GC_DEF_CTNR_BEGIN_FUNC_PTR

#undef GC_DEF_CTNR_CLEAR_FUNC_PTR

#undef GC_DEF_CTNR_END_FUNC_PTR

#undef GC_DEF_CTNR_ERASE_FUNC_PTR

#undef GC_DEF_CTNR_FRONT_FUNC_PTR

#undef GC_DEF_CTNR_INSERT_AFTER_FUNC_PTR

#undef GC_DEF_CTNR_INSERT_BEFORE_FUNC_PTR

#undef GC_DEF_CTNR_POP_BACK_FUNC_PTR

#undef GC_DEF_CTNR_POP_FRONT_FUNC_PTR

#undef GC_DEF_CTNR_PUSH_BACK_FUNC_PTR

#undef GC_DEF_CTNR_PUSH_FRONT_FUNC_PTR

#undef GC_DEF_CTNR_RBEGIN_FUNC_PTR

#undef GC_DEF_CTNR_REND_FUNC_PTR

#undef GC_DEF_CTNR_RESERVE_FUNC_PTR

#undef GC_DEF_CTNR_RESIZE_FUNC_PTR

#undef GC_DECL_CTNR_FUNC
