# genc #

### Purpose ###

When writing code in C, one can often spend a lot of time managing collections, memory etc. The genc library aims to provide a C++ STL like set of containers and algorithms.

### Aims ####

* To offer generic, data agnostic implementations of common containers.
* To decouple algorithms from specific containers.
* To provide a uniform interface for iterating over containers.
* To provide a cross-platform solution.

### How do I get set up? ###

* Use CMake to generate the necessary build files.
* Use the build files generated by CMake to build the library.
* Use the examples to familiarise yourself with the interface and learn how to use the library.
* Ensure Doxygen is installed to generate API documentation via CMake doc target.

### Status ###
* First release out (19.3.4.1).
* Coding guidelines for contributions required.
* Feedback welcome.
